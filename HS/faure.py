from win32com.client import Dispatch
import win32com.client as win32
import shutil
import os


DATA_FILE = r'\\bank\Logistyka\Logistyka_Kopia\Backup 1\latest\homes\Logistyka\Faurecia\Faurecia dostawy, wysyłki i etykiety 2022 DOBRY!!!.xlsx'


def editXLSX(excel):
    workbook = excel.Workbooks.Open(DATA_FILE)
    sheet = workbook.Sheets('Sample card')
    new_sheet = workbook.Sheets('przewodnik faure')

    new_sheet.Cells.ClearContents()
    new_sheet.Cells.ClearFormats()
    
    new_sheet.Columns('A').ColumnWidth = 8.43
    new_sheet.Columns('B').ColumnWidth = 8.43
    new_sheet.Columns('C').ColumnWidth = 13
    new_sheet.Columns('D').ColumnWidth = 11.71
    new_sheet.Columns('E').ColumnWidth = 19.14
    new_sheet.Columns('F').ColumnWidth = 8.43
    new_sheet.Columns('G').ColumnWidth = 8.43
    new_sheet.Columns('H').ColumnWidth = 33.57

    for i in range(2, 63):
        print(i)
        sheet.Range('E6').Formula = f"='do etykiet'!$C${i}"
        sheet.Range('E8').Formula = f"='do etykiet'!$E${i}"
        sheet.Range('E9').Formula = f"='do etykiet'!$F${i}"
        sheet.Range('C11').Formula = f"='do etykiet'!$I${i}"
        sheet.Range('F11').Value = f"='do etykiet'!$H${i}"
        sheet.Range('F13').Value = f"='do etykiet'!$G${i}"
        sheet.Range('F15').Value = f"='do etykiet'!$A${i}"

        new_sheet.Rows((i-2)*22+1).RowHeight = 25.5
        new_sheet.Rows((i-2)*22+2).RowHeight = 25.5
        new_sheet.Rows((i-2)*22+3).RowHeight = 15
        new_sheet.Rows((i-2)*22+4).RowHeight = 24.75
        new_sheet.Rows((i-2)*22+5).RowHeight = 24.5
        new_sheet.Rows((i-2)*22+6).RowHeight = 31.5
        new_sheet.Rows((i-2)*22+7).RowHeight = 14.25
        new_sheet.Rows((i-2)*22+8).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+9).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+10).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+11).RowHeight = 33
        new_sheet.Rows((i-2)*22+12).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+13).RowHeight = 27.75
        new_sheet.Rows((i-2)*22+14).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+15).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+16).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+17).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+18).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+19).RowHeight = 23.25
        new_sheet.Rows((i-2)*22+20).RowHeight = 15
        new_sheet.Rows((i-2)*22+21).RowHeight = 15
        new_sheet.Rows((i-2)*22+22).RowHeight = 60.75
        
        sheet.Range('A1:H22').Copy(new_sheet.Range(f'A{(i-2)*22+1}:H{(i-2)*22+22}'))
        
        

if __name__ == "__main__":
    xlsx = Dispatch('Excel.Application')
    editXLSX(xlsx)

import re
from datetime import datetime, timedelta

import pandas
import win32com.client as win32
from config import *
from win32com.client import Dispatch


def get_data():
    today = datetime.today()
    days_until_next_thursday = (3 - today.weekday()) % 7
    next_thursday = today + timedelta(days=days_until_next_thursday)
    next_thursday_str = next_thursday.strftime('%d.%m.%Y')
    
    data_list = []
    data = pandas.read_excel(RAW_FILE, sheet_name=0, usecols=[0,1,2,3,6,7])
    data['Data wysyłki z Hutchinson'] = data['Data wysyłki z Hutchinson'].dt.strftime('%d.%m.%Y')
    first = True
    for index, row in data.iterrows():
        if first:
            with open(CONFIG_FILE, 'r') as file:
                lines = file.readlines()
                first = False
            with open(CONFIG_FILE, 'w') as file:
                for line in lines:
                    new_line = re.sub(r'DATE\s*=\s*(.*)', f'DATE = "{row[0]}"', line)
                    file.write(new_line)

        n_containers = row[4]
        date = row[0]
        article = row[1]
        order = row[2]
        section = row[3]
        code = row[5]
        for _ in range(n_containers):
            data_list.append(['', date, '', article, section, '', 'Karton', 'OONOO', order, code, next_thursday_str])        
    
    
    return data_list


def write_data(excel, data):
    workbook = excel.Workbooks.Open(DATA_FILE)
    sheet = workbook.Sheets('Dostawa ')
    print('last_row', LAST_ROW)
    last_row = LAST_ROW
    for row in data:
        last_row += 1
        iter = 0
        row[0] = last_row
        for i, value in enumerate(row):
            sheet.Cells(last_row, i + 1).Value = value
            if i != 0:
                sheet.Cells(last_row, i + 1).Borders.LineStyle = 1
            iter = i + 2

        sheet.Cells(last_row, iter).Formula = f'=CONCATENATE(C{last_row}, "-", TEXT(K{last_row}, "dd.mm.rrrr"))'
        sheet.Cells(last_row, iter).Borders.LineStyle = 1

    with open(CONFIG_FILE, 'r') as file:
        lines = file.readlines()
    with open(CONFIG_FILE, 'w') as file:
        for line in lines:
            new_line = re.sub(r'LAST_ROW\s*=\s*(.*)', f'LAST_ROW = {last_row}', line)
            file.write(new_line)

    print('last_row', last_row)
    workbook.Save()
    workbook.Close()
    

if __name__ == "__main__":
    excel = Dispatch('Excel.Application')
    data = get_data()
    write_data(excel, data)

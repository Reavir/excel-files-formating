from datetime import datetime
import re
import win32com.client as win32
from config import *
from win32com.client import Dispatch
import sys
sys.path.insert(0, '..')
import utils.excel


def readData(excel):
    workbook = excel.Workbooks.Open(DATA_FILE)

    sheet = workbook.Sheets('Dostawa ')

    row = LAST_ROW

    data = []
    while True:
        if sheet.Cells(row, 3).Value in [None, 0]:
            break
        print(row)

        row_data = []
        for column in range(1, sheet.UsedRange.Columns.Count + 1):
            row_data.append(sheet.Cells(row, column).Value)

        data.append(row_data)
        row += 1

    with open(CONFIG_FILE, 'r') as file:
        lines = file.readlines()
    with open(CONFIG_FILE, 'w') as file:
        for line in lines:
            new_line = re.sub(r'LAST_ROW\s*=\s*(.*)', f'LAST_ROW = {row}', line)
            file.write(new_line)
            
    return data


def editXLSX(excel, db, pdf):
    excel_app = utils.excel.open_excel_app()
    sheet = utils.excel.open_worksheet(LABEL_FILE, excel_app, sheet='Sample')
    new_sheet = utils.excel.open_worksheet(LABEL_FILE, excel_app, sheet='Labels')

    new_sheet.Cells.ClearContents()
    new_sheet.Cells.ClearFormats()
    
    new_sheet.Columns('A').ColumnWidth = 16.86
    new_sheet.Columns('B').ColumnWidth = 15.29
    new_sheet.Columns('C').ColumnWidth = 32.86
    for i, row in enumerate(db):
        '''
            Article number
            Kod obróbki
            Order ref.
            Hutchinson section number
            Quantity
            Delivery date
            Hutchinson code
            Master label - Production Date
        '''
        
        sheet.Range('B4').Value = row[2]
        sheet.Range('B5').Value = row[7]
        sheet.Range('B6').Value = row[8]
        sheet.Range('B8').Value = row[4]
        sheet.Range('B9').Value = row[5]
        sheet.Range('B10').Value = row[1]
        sheet.Range('B11').Value = row[9]
        sheet.Range('B12').Value = row[11]
        

        if pdf:
            print(f'{db[i][11]}.pdf')
            sheet.ExportAsFixedFormat(0, OUTPUT_FILE + f'\{db[i][11]}.pdf')
        
        else:
            print(f'{db[i][11]}')
            sheet.Range('A4:C23').Copy(new_sheet.Range(f'A{i*23+4}:C{i*23+23}'))
            if i > 87:
                sheet.Range('A1:C3').Copy(new_sheet.Range(f'A{i*23+1}:C{i*23+3}'))
                new_sheet.Rows(i*23+1).RowHeight = 15
                new_sheet.Rows(i*23+2).RowHeight = 15
                new_sheet.Rows(i*23+3).RowHeight = 15
                new_sheet.Rows(i*23+4).RowHeight = 53.25
                new_sheet.Rows(i*23+5).RowHeight = 61.5
                new_sheet.Rows(i*23+6).RowHeight = 15.75
                new_sheet.Rows(i*23+7).RowHeight = 39.75
                new_sheet.Rows(i*23+8).RowHeight = 35.25
                new_sheet.Rows(i*23+9).RowHeight = 54.75
                new_sheet.Rows(i*23+10).RowHeight = 30
                new_sheet.Rows(i*23+11).RowHeight = 30
                new_sheet.Rows(i*23+12).RowHeight = 47.25
                new_sheet.Rows(i*23+13).RowHeight = 42
                new_sheet.Rows(i*23+14).RowHeight = 15
                new_sheet.Rows(i*23+15).RowHeight = 15
                new_sheet.Rows(i*23+16).RowHeight = 15
                new_sheet.Rows(i*23+17).RowHeight = 15
                new_sheet.Rows(i*23+18).RowHeight = 15
                new_sheet.Rows(i*23+19).RowHeight = 15
                new_sheet.Rows(i*23+20).RowHeight = 15
                new_sheet.Rows(i*23+21).RowHeight = 15
                new_sheet.Rows(i*23+22).RowHeight = 15
                new_sheet.Rows(i*23+23).RowHeight = 15
                new_sheet.Rows(i*23+24).RowHeight = 15
                new_sheet.Rows(i*23+25).RowHeight = 15


if __name__ == "__main__":
    xlsx = Dispatch('Excel.Application')
    data =  readData(xlsx)
    editXLSX(xlsx, data, pdf=PDF)


import requests
import csv
import sys
from PIL import Image


def extract_text_from_image(image_path, api_key):
    # OCR.space API endpoint
    api_url = 'https://api.ocr.space/parse/image'

    # Request payload
    payload = {
        'apikey': api_key,
        'isOverlayRequired': False,
        'OCREngine': 3,
        'isTable': True

    }

    # Read the image file as binary data
    with open(image_path, 'rb') as image_file:
        # Send a POST request to OCR.space API with the image file
        response = requests.post(api_url, files={'image': image_file}, data=payload)

        # Parse the JSON response
        result = response.json()

        if response.status_code == 200 and result.get('OCRExitCode') == 1:
            # OCR succeeded, extract the parsed text
            parsed_text = result.get('ParsedResults')[0].get('ParsedText')

            # Return the parsed text
            return parsed_text
        else:
            # OCR failed, return None
            return None


def write_processed_text_to_csv(text, output_file, step):
    if step == 1:
        text = data_with_L7(text)
        # Mubea
        with open(output_file[0], 'w+', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            for i in range(len(text[0][0])):
                csvwriter.writerow([text[0][0][i], text[0][1][i], text[0][2][i]])
        # Faurecia
        with open(output_file[1], 'w+', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            for i in range(len(text[1][0])):
                csvwriter.writerow([text[1][0][i], text[1][1][i]])

    else:
        # Mubea
        with open(output_file[0], 'w+', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            for i in range(len(text[0][0])):
                csvwriter.writerow([text[0][0][i], text[0][1][i]])
        # Faurecia
        with open(output_file[1], 'w+', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';')
            for i in range(len(text[1][0])):
                csvwriter.writerow([text[1][0][i], text[1][1][i]])


def data_with_L7(data):
    col_1 = []
    col_2 = []
    file = list(csv.reader(open('output_mubea.csv', 'r')))
    for element in file:
        col_1.append(element[0].split(';')[0])
        col_2.append(element[0].split(';')[1])

    col_3 = [''] * len(col_1)
    for i in range(len(data[0][0])):
        if data[0][0][i] in col_1:
            col_3[col_1.index(data[0][0][i])] = data[0][1][i]
        else:
            col_1.append(data[0][0][i])
            col_2.append('')
            col_3.append(data[0][1][i])

    col_4 = []
    col_5 = [] 
    file = list(csv.reader(open('output_faurecia.csv', 'r')))
    for element in file:
        col_4.append(element[0].split(';')[0])
        col_5.append(element[0].split(';')[1])

    for i in range(len(data[1][0])):
        if data[1][0][i] in col_1:
            col_5[col_4.index(data[1][0][i])] += data[1][1][i]
        else:
            col_4.append(data[1][0][i])
            col_5.append(data[1][1][i])

    return [[col_1, col_2, col_3], [col_4, col_5]]


def find_numerical(string):
    num_string = ""
    for i in range(len(string)):
        if string == '014' :
            return num_string

        if string[i].isdigit():
            num_string += string[i]
        elif string[i] == 'O':
            num_string += '0'
        elif string[i] == ' ':
            return num_string

        if len(num_string) > 3:
            return num_string
        elif len(num_string) == 3 and i > 3:
            return num_string
        elif len(num_string) == 3 and len(string) == 3:
            return num_string


def process_text(raw_text):
    with open('id/id_mubea.txt') as h:
        m = h.read().splitlines()
    with open('id/id_faurecia.txt') as g:
        f = g.read().splitlines()

    faurecia = []
    mubea = []

    for row in raw_text:                
        if 'Faurecia' in row[0] or 'Faure' in row[0] or 'falrecia' in row[0]:
            faurecia.append([find_numerical(row[0]), row[1]])  
        elif 'MFS' in row[0]:
            faurecia.append(['2914063X', row[1]])
        elif 'G6X' in row[0].upper() or 'GEX' in row[0].upper():
            faurecia.append(['2870784', row[1]])
        else:
            mubea.append([find_numerical(row[0]), row[1]])

    for id_list, full_id_list in zip([mubea, faurecia], [m, f]):
        for id in id_list:
            if '2914063X' not in id[0] and '2870784' not in id[0]:
                strng_slicer = -1 * len(id[0])
                full_id = next((x for x in full_id_list if x[strng_slicer:] == id[0]), None)
                id[0] = full_id
    
    mubea_sigma = {}
    faure_sigma = {}

    for row in mubea:
        if row[0] in mubea_sigma:
            mubea_sigma[row[0]] += "+" + row[1]
        else:
            mubea_sigma[row[0]] = "=" + row[1]

    for row in faurecia:
        if row[0] in faure_sigma:
            faure_sigma[row[0]] += "+" + row[1]
        else:
            faure_sigma[row[0]] = "=" + row[1]
    
    return [[list(mubea_sigma.keys()), list(mubea_sigma.values())], [list(faure_sigma.keys()), list(faure_sigma.values())]]


def scale_image_to_resolution(image_path, target_resolution, target_file):
    # Open the image
    image = Image.open(image_path)

    # Get the current resolution (dpi) as a tuple (x, y)
    current_resolution = image.info['dpi']
    current_resolution_x, current_resolution_y = current_resolution

    # Calculate the scaling factors for width and height
    scale_factor_x = target_resolution / current_resolution_x
    scale_factor_y = target_resolution / current_resolution_y

    # Calculate the new dimensions of the image based on the scale factors
    new_width = round(image.width * scale_factor_x)
    new_height = round(image.height * scale_factor_y)

    # Resize the image using the calculated dimensions
    resized_image = image.resize((new_width, new_height), Image.BICUBIC)

    # Update the image resolution to the target resolution
    resized_resolution = (target_resolution, target_resolution)
    resized_image.info['dpi'] = resized_resolution

    # Save the resized image
    resized_image.save(target_file)


if __name__ == "__main__":
    
    # Path to screenshot image
    image_path = ['ss/scaled_L1.png', 'ss/scaled_L7.png']

    # Path to the output
    raw_output_path = ['raw_L1.csv', 'raw_L7.csv']
    output_path = ['output_mubea.csv', 'output_faurecia.csv']

    # OCR.space API key
    api_key = 'K83373162788957'
    #extracted_text = []

    # Extract text from the image
    if sys.argv[1] == 'scan':  
        scale_image_to_resolution('ss/ssL1.png', 400, 'ss/scaled_L1.png')
        scale_image_to_resolution('ss/ssL7.png', 400, 'ss/scaled_L7.png')
        for image, raw_output_file in zip(image_path, raw_output_path):
            raw_txt = extract_text_from_image(image, api_key)
            lines = raw_txt.split('\n')
            rows = [line.strip().split('\t') for line in lines if line.strip()]

            with open(raw_output_file, 'wt', encoding='utf-8', newline='') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=';')
                for row in rows:
                    row = row + [''] if len(row) != 2 else row
                    csv_writer.writerow(row)
    
    # Tests without API
    elif sys.argv[1] == 'text':
        for step, raw_output_file in enumerate(raw_output_path):
            extracted_text = list(csv.reader(open(raw_output_file, 'r'), delimiter=';'))
            processed_text = process_text(extracted_text)
            write_processed_text_to_csv(processed_text, output_path, step)

    
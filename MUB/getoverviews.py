import os
import pandas as pd
import re
import warnings

warnings.filterwarnings("ignore", message="Cannot parse header or footer so it will be ignored", category=UserWarning)

INPUT_PATH = r'C:\Users\Logistyka PL\Desktop\MUBEA\overviews'

def extract_date(lst, last_index):
    date_indices = []
    date_pattern = r'\d{2}\.\d{2}\.\d{4}'
    for idx, item in enumerate(lst):
        match = re.search(date_pattern, item)
        if match and 'Loaded' not in item:
            if idx != last_index:
                date_indices.append(item)

    return date_indices

production_per_day_per_ident = {}

# Get a list of all files in the folder
files = os.listdir(INPUT_PATH)
files.sort(reverse=False)
for i, file in enumerate(files, start=1):

    print(i, file)
    file_path = INPUT_PATH + '\\' + file

    column_names = pd.read_excel(
            file_path,
            sheet_name='Quantity',
            header=6,
        ).columns
        
    columns_list = column_names.str.strip().to_list()

    selected_columns = [
        'Part No.',
    ] + extract_date(columns_list, len(column_names) - 1)

    raw_data = pd.read_excel(
        file_path,
        sheet_name='Quantity',
        header=6,
        usecols=selected_columns
    )


    days = selected_columns[1:]
    for day in days:
        print(day)
        if day not in production_per_day_per_ident:
            production_per_day_per_ident[day] = {}
        for index, row in raw_data.iterrows():
            if row['Part No.'] == 'Total' or row['Part No.'] == 'Celkem':
                break
            if pd.isna(row['Part No.']):
                continue

            ident = int(row['Part No.'])
            value = row[day]
            if not pd.isna(value):
                try:
                    if int(value) != 0:
                        production_per_day_per_ident[day][ident] = production_per_day_per_ident[day].get(ident, 0) + int(value)
                except ValueError:
                    continue

print(production_per_day_per_ident)

# Create a DataFrame from the data
df = pd.DataFrame(production_per_day_per_ident)
print(df)
# Transpose the DataFrame to have dates as rows and columns as user IDs
df = df.transpose()

# Convert the index (dates) to datetime objects
df.index = pd.to_datetime(df.index, format='%d.%m.%Y')
df = df.transpose()

print("==-=-=-=-")
print("==-=-=-=-")
print("==-=-=-=-")
print("==-=-=-=-")
print("==-=-=-=-")
print(df)
# Group data by European calendar week (ISO week) and sum the values
df_grouped = df

# Reset the index to include the week number as a column
df_grouped.reset_index(inplace=True)
print(df_grouped)


# Write the grouped data to a CSV file
df_grouped.to_csv('week_data.csv', index=False, sep=';')


df = pd.read_csv('week_data.csv', sep=';')

# Loop through each column and remove '.0' from numeric values
for column in df.columns:
    if df[column].dtype == 'float64':
        df[column] = df[column].apply(lambda x: int(x) if x.is_integer() else x)

# Save the modified DataFrame back to the CSV file
df.to_csv('week_data_two.csv', sep=';', index=False)
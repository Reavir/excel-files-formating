import subprocess
import sys
import time

def init():
    start = time.time()

    try:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', '-r', 'requirements.txt'])
        print("All required packages have been successfully installed or updated.")
    except Exception as e:
        print("An error occurred while installing/updating requirements:", e)
        sys.exit(1)

    end = time.time()

    print('Checking took: ', end - start, ' s')

if __name__ == "__main__":
    init()

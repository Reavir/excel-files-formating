import datetime
import re
import sys

import pandas as pd

sys.path.insert(0, '..')
from config import *

from utils.excel import *


def read_data_production(input_date):
    #                               #
    #                               #
    #                               #
    # Read data from total quantity #
    #                               #
    #                               #
    #                               #
    print(f'Reading from {TOTAL_QUANTITY_FILE}')
    column_names = pd.read_excel(TOTAL_QUANTITY_FILE, sheet_name='Production ', nrows=0).columns
    selected_columns = \
            [column_names[0]] + \
            [column_names[1]] + \
            [column_names[3]] + \
            [column_names[4]] + \
            [column_names[8]] + \
            [column_names[14]] + \
            [column_names[20]]
    
    prod_data = pd.read_excel(
        TOTAL_QUANTITY_FILE,
        sheet_name='Production ',
        usecols=selected_columns,
    )
    
    produced_type_day = {
        'Stange PROD': {},
        'Bügel   PROD': {}
    }
    produced_cw = {}
    first_dat = datetime.datetime.strptime(input_date, '%d.%m.%Y')
    # Convert the datetime object to a timestamp
    for index, row in prod_data.iterrows():
        if PRODUCTION_CW != '':
            cw = int(PRODUCTION_CW)
        else:
            cw = datetime.datetime.now().isocalendar()[1]

        line_1 = row[4] if not pd.isna(row[4]) else 0
        line_2 = row[5] if not pd.isna(row[5]) else 0
        line_7 = row[6] if not pd.isna(row[6]) else 0

        if row[3] == cw and row[2].year == datetime.datetime.now().year:
            produced_cw[row[0]] = produced_cw.get(row[0], 0) + line_1 + line_2 + line_7

        if row[2] >= first_dat:
            if 'Bügel' in row[1]:
                s_type = 'Bügel   PROD'
            elif 'Stange' in row[1]:
                s_type = 'Stange PROD'

            produced_type_day[s_type][row[2].strftime('%d.%m.%Y')] = produced_type_day[s_type].get(row[2].strftime('%d.%m.%Y'), 0) + line_1 + line_2 + line_7
    
    return produced_cw, produced_type_day, cw


def read_data_plan():
    df = pd.read_excel(
        OUTPUT_PRODUCTION_PLAN_TABLE_FILE,
        sheet_name='Wygenerowany',
        header=12
    )

    date_columns = []

    for column in df.columns:
        if isinstance(column, str):
            try:
                date = datetime.datetime.strptime(column, '%d.%m.%Y')
                date_columns.append(date)
            except ValueError:
                try:
                    date = datetime.datetime.fromisoformat(column)
                    date_columns.append(date)
                except ValueError:
                    pass
        elif isinstance(column, datetime.datetime):
            date_columns.append(column)

    date_strings = [date.strftime('%d.%m.%Y') for date in date_columns]

    return date_strings


def update_excel(produced_cw, produced_type_day, cw):
    excel_app = open_excel_app()
    sheet = open_worksheet(OUTPUT_PRODUCTION_PLAN_TABLE_FILE, excel_app, sheet='Wygenerowany')
    isFound = False
    col = 1
    while not isFound:
        if re.match(r'KW\d+', str(sheet.Cells(13, col).Value)):
            isFound = True
            break 

        col += 1

    sheet.Cells(13, col).Value = "KW" + str(cw)

    for ident, val in zip(produced_cw.keys(), produced_cw.values()):
        row = sheet.Columns(1).Find(ident).Row
        sheet.Cells(row, col).Value = val

    for s_type, days in zip(produced_type_day.keys(), produced_type_day.values()):
        row = sheet.Columns(2).Find(s_type).Row
        for date, val in zip(days.keys(), days.values()):
            try:
                day_col = sheet.Rows(13).Find(date).Column
            except AttributeError:
                date = datetime.datetime.strptime(date, '%d.%m.%Y')
                day_col = sheet.Rows(13).Find(date).Column

            sheet.Cells(row, day_col).Value = val


if __name__ == "__main__":
    date_strings = read_data_plan()
    produced_cw, produced_type_day, cw = read_data_production(date_strings[0])
    update_excel(produced_cw, produced_type_day, cw)
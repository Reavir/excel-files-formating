import copy
import datetime
import re
import string
import sys
import time
import warnings
from math import ceil, floor

import pandas as pd
import win32com.client as win32
from config import *
from tqdm import tqdm

sys.path.insert(0, '..')
from utils.excel import *
import win32com
import os
print(win32com.__gen_path__)
#os.remove(win32com.__gen_path__)

def read_data_excel():
    warnings.filterwarnings("ignore", message="Cannot parse header or footer so it will be ignored", category=UserWarning)
    date_pattern = r'\d{2}\.\d{2}\.\d{4}'

    def convert_to_date(dtxt):   
        match = re.search(date_pattern, dtxt)
        if match:
            extracted_date = match.group()

        date_format = "%d.%m.%Y"
        date = datetime.datetime.strptime(extracted_date, date_format)
        previous_day = date - datetime.timedelta(days=2)

        return previous_day.weekday() + 1, previous_day.strftime(date_format)
    

    def extract_date(lst, last_index):
        date_indices = []

        for idx, item in enumerate(lst):
            match = re.search(date_pattern, item)
            if match and 'Loaded' not in item:
                if idx != last_index:
                    date_indices.append(item)

        return date_indices


    #                        #
    #                        #
    #                        #
    # Data from Raw material #
    #                        #
    #                        #
    #                        #

    print(f'Reading from {CURRENT_RAW_FILE}')
    raw_data = pd.read_excel(CURRENT_RAW_FILE, sheet_name='aktualny raw', usecols=[6, 9], skiprows=range(1, 11))

    supply_per_ident = {}
    for index, row in raw_data.iterrows():
        if pd.isna(row[0]):
            break

        ident = int(row[0])
        value = int(row[1])

        if value == 0:
            continue
        
        supply_per_ident[ident] = supply_per_ident.get(ident, 0) + value

    #                        #
    #                        #
    #                        #
    # Data from GTM Overview #
    #                        #
    #                        #
    #                        #
    column_names = pd.read_excel(
        MUBEA_PRODUCTION_TABLE_FILE,
        sheet_name='Quantity',
        header=6,
    ).columns
    
    columns_list = column_names.str.strip().to_list()

    selected_columns = [
        'Part No.',
        'Part description',
        'backlog'
    ] + extract_date(columns_list, len(column_names) - 1)
    
    raw_data = pd.read_excel(
        MUBEA_PRODUCTION_TABLE_FILE,
        sheet_name='Quantity',
        header=6,
        usecols=selected_columns
    )
    print(f'Reading from {MUBEA_PRODUCTION_TABLE_FILE}')
    production_per_day_per_ident = {}
    production_per_ident = {}
    backlog = {}
    name = {}
    i = True

    rng = selected_columns[3:]
    old_day_nr, old_day_string = convert_to_date(selected_columns[3]) 
    old_day_nr -= 1
    for dtxt in rng:
        # Convert to previous day
        day_nr, day_string = convert_to_date(dtxt)
        while abs(day_nr - old_day_nr) != 1 and abs(day_nr - old_day_nr) != 6 and day_nr != old_day_nr:
            old_day_nr += 1
            date_object = datetime.datetime.strptime(old_day_string, '%d.%m.%Y')
            next_day = date_object + datetime.timedelta(days=1)
            old_day_string = next_day.strftime('%d.%m.%Y')

            production_per_day_per_ident[(old_day_nr, old_day_string)] = {}
            rng.pop()

        production_per_day_per_ident[(day_nr, day_string)] = {}
        if i == True:
            first_day = day_string
        for index, row in raw_data.iterrows():
            if row['Part No.'] == 'Total' or row['Part No.'] == 'Celkem':
                break
            if pd.isna(row['Part No.']):
                continue

            ident = int(row['Part No.'])
            if ident not in supply_per_ident:
                supply_per_ident[ident] = 0

            value = row[dtxt]
            # Don't iterate for every day, do it only once
            if i == True:
                value_backlog = int(row['backlog']) if not pd.isna(row['backlog']) else 0
                backlog[ident] = value_backlog

                name[ident] = row['Part description']

            if not pd.isna(value):
                if int(value) != 0:
                    production_per_ident[ident] = production_per_ident.get(ident, 0) + int(value)
                    production_per_day_per_ident[(day_nr, day_string)][ident] = production_per_day_per_ident[(day_nr, day_string)].get(ident, 0) + int(value)
        
        old_day_nr = day_nr
        old_day_string = day_string
        i = False
    
    #                          #
    #                          #
    #                          #
    # Read data from spec file #
    #                          #
    #                          #
    #                          #
    print(f'Reading from {CURRENT_SPEC_FILE}')
    column_names = pd.read_excel(CURRENT_SPEC_FILE, sheet_name='nowa specyfikacja ', header=4, nrows=0).columns
    selected_columns = \
            [column_names[1]] + \
            [column_names[5]] + \
            [column_names[7]]
    spec_data = pd.read_excel(
        CURRENT_SPEC_FILE,
        sheet_name='nowa specyfikacja ',
        header=4,
        usecols=selected_columns,
    )
    spec_data[column_names[1]] = pd.to_datetime(spec_data[column_names[1]], format='%d.%m.%Y')
    if SHIPMENT != '':
        filtered_data = spec_data[spec_data[column_names[1]] >= pd.to_datetime(SHIPMENT, format='%d.%m.%Y')]
    else:
        filtered_data = spec_data[spec_data[column_names[1]] >= pd.to_datetime(first_day, format='%d.%m.%Y')]
    
    delivery_per_ident = {}
    delivery_per_day_per_ident = {}
    first = True if SHIPMENT == '' else False
    for index, row in filtered_data.iterrows():
        if first is True:
            first_date = row[0].strftime('%d.%m.%Y')
        else:
            first_date = False
        first = False
        if row[0].strftime('%d.%m.%Y') not in delivery_per_day_per_ident:
            delivery_per_day_per_ident[row[0].strftime('%d.%m.%Y')] = {}
        if pd.isna(row[2]):
            continue
        if row[0].strftime('%d.%m.%Y') != first_date:
            delivery_per_ident[row[1]] = delivery_per_ident.get(row[1], 0) + int(row[2])

        delivery_per_day_per_ident[row[0].strftime('%d.%m.%Y')][row[1]] = delivery_per_day_per_ident[row[0].strftime('%d.%m.%Y')].get(row[1], 0) + int(row[2])
        
    #                               #
    #                               #
    #                               #
    # Read data from total quantity #
    #                               #
    #                               #
    #                               #
    print(f'Reading from {TOTAL_QUANTITY_FILE}')
    column_names = pd.read_excel(TOTAL_QUANTITY_FILE, sheet_name='Production ', nrows=0).columns
    selected_columns = \
            [column_names[0]] + \
            [column_names[1]] + \
            [column_names[3]] + \
            [column_names[4]] + \
            [column_names[8]] + \
            [column_names[14]] + \
            [column_names[20]]
    prod_data = pd.read_excel(
        TOTAL_QUANTITY_FILE,
        sheet_name='Production ',
        usecols=selected_columns,
    )
    #skiprows=range(1, 11700)
    
    produced_type_day = {
        'Stange PROD': {},
        'Bügel   PROD': {}
    }
    produced_cw = {}
    first_dat = datetime.datetime.strptime(sys.argv[1], '%d.%m.%Y')
    # Convert the datetime object to a timestamp
    for index, row in prod_data.iterrows():
        if PRODUCTION_CW != '':
            cw = int(PRODUCTION_CW)
        else:
            cw = datetime.datetime.now().isocalendar()[1]

        line_1 = row[4] if not pd.isna(row[4]) else 0
        line_2 = row[5] if not pd.isna(row[5]) else 0
        line_7 = row[6] if not pd.isna(row[6]) else 0

        if row[3] == cw and row[2].year == datetime.datetime.now().year:
            produced_cw[row[0]] = produced_cw.get(row[0], 0) + line_1 + line_2 + line_7

        if row[2] >= first_dat:
            if 'Bügel' in row[1]:
                s_type = 'Bügel   PROD'
            elif 'Stange' in row[1]:
                s_type = 'Stange PROD'

            produced_type_day[s_type][row[2].strftime('%d.%m.%Y')] = produced_type_day[s_type].get(row[2].strftime('%d.%m.%Y'), 0) + line_1 + line_2 + line_7
    
    return supply_per_ident, production_per_day_per_ident, backlog, name, first_day, delivery_per_day_per_ident, production_per_ident, delivery_per_ident, produced_cw, produced_type_day


def read_data_prod_plan():
    pendant_cap_data = pd.read_excel(
        OUTPUT_PRODUCTION_PLAN_TABLE_FILE,
        sheet_name='Zawieszki',
        dtype=str
    )
    print(f'Reading from {OUTPUT_PRODUCTION_PLAN_TABLE_FILE} Zawieszki')
    
    pendant_capacity = {}
    for index, row in pendant_cap_data.iterrows():
        max_pendant_capacity = floor(int(row[1]) * int(row[2]) * 24 / 2.75 * PRODUCTIVITY)
        if max_pendant_capacity != 0 :
            pendant_capacity[int(row[0])] = max_pendant_capacity
        
    id_quantity_data = pd.read_excel(
        OUTPUT_PRODUCTION_PLAN_TABLE_FILE,
        sheet_name='Dostawcy MUBEA',
        dtype=str
    )
    print(f'Reading from {OUTPUT_PRODUCTION_PLAN_TABLE_FILE} Dostawcy MUBEA')

    id_quantity_pendant = {}
    id_quantity_line = {}
    line_capacity = {}
    line_capacity[1] = L_1_CAPACITY
    line_capacity[2] = L_2_CAPACITY
    for index, row in id_quantity_data.iterrows():
        if not pd.isna(row[2]):
            if '.' in row[2]:
                day = max([int(e) for e in row[2].split('.')])
            elif ',' in row[2]:
                day = max([int(e) for e in row[2].split(',')])
            else:
                day = int(row[2])
        else:
            day = False

        if int(row[7]) in id_quantity_line:
            id_quantity_line[int(row[7])][int(row[0])] = [int(row[1]), day, row[3]]
        else:
            id_quantity_line[int(row[7])] = {}
            id_quantity_line[int(row[7])][int(row[0])] = [int(row[1]), day, row[3]]
    
        if pd.isna(row[4]):
            continue
        if '.' in row[4]:
            pendants = [int(pen) for pen in row[4].split('.')]
        elif ',' in row[4]:
            pendants = [int(pen) for pen in row[4].split(',')]
        else:
            pendants = [int(row[4])]

        for pendant in pendants:
            if pendant in id_quantity_pendant:
                id_quantity_pendant[pendant][int(row[0])] = [int(row[1]), day, row[3]]
            else:
                id_quantity_pendant[pendant] = {}
                id_quantity_pendant[pendant][int(row[0])] = [int(row[1]), day, row[3]]

    return id_quantity_pendant, pendant_capacity, line_capacity, id_quantity_line


def create_plan():
    supply_per_ident, production_per_day_per_ident, backlog, name, first_day, delivery_per_day_per_ident, production_per_ident, delivery_per_ident, produced_cw, produced_type_day = read_data_excel()
    id_quantity_pendant, pendant_capacity, line_capacity, id_quantity_line = read_data_prod_plan()
    supply_copy = supply_per_ident.copy()
    backlog_copy = backlog.copy()
    delivery_per_day_per_ident_copy = delivery_per_day_per_ident.copy()
    print('Counting...')
    '''
        supply_per_ident = {
            id: supply
        }

        production_per_day_per_ident = {
            (day_nr, day_string): {
                id: production_plan
            } 
        } 

        backlog = {
            id: backlog      # All IDs, 0 if no backlog
        } 
        
        id_quantity_pendant = {
            pendant: {
                id: [multiplier, if number 'not producing that day_nr', customer]
            }
        } 

        id_quantity_line = {
            line: {
                id: [multiplier, if number 'not producing that day_nr', customer]
        } 
        

        pendant_capacity = {
            pendant: [capacity, quantity]
        }

        pendant_day_capacity = {
            day: {
                pendant: [capacity, quantity]
            }
        }
        output_data = {
            (day_nr, day_string): {
                (id, pendant): value
            }
        }
        name = {
            id: name
        }
        delivery_per_day_per_ident = {
            day_string: {
                ident: value
            }
        }
    '''

    #pendant_day_capacity = {day: {} for day in production_per_day_per_ident.keys()}
    ## Populate the pendant_day_capacity dictionary
    #for day in production_per_day_per_ident.keys():
    #    for pendant in pendant_capacity.keys():
    #        if day[1] in SHIFTS:
    #            pendant_day_capacity[day][pendant] = ceil(pendant_capacity[pendant] * SHIFTS[day[1]] / 3)
    #        else:
    #            pendant_day_capacity[day][pendant] = pendant_capacity[pendant]



    # Order the idents in dictionary
    #for pendant, pendant_values in zip(id_quantity_pendant.keys(), id_quantity_pendant.values()):
    #    pendant_values = dict(sorted(pendant_values.items(), key=lambda x: x[1][0], reverse=True))
    #    pendant_values = dict(sorted(pendant_values.items(), key=lambda x: (x[1][2] not in IMPORTANT_CUSTOMERS_MUBEA, IMPORTANT_CUSTOMERS_MUBEA.index(x[1][2]) if x[1][2] in IMPORTANT_CUSTOMERS_MUBEA else float('inf'))))
    #    pendant_values = dict(sorted(pendant_values.items(), key=lambda x: (x[0] not in CRITICAL_PARTS, CRITICAL_PARTS.index(x[0]) if x[0] in CRITICAL_PARTS else float('inf'))))
    #
    #    id_quantity_pendant[pendant] = pendant_values
    # Order the idents in dictionary
    for line, line_values in zip(id_quantity_line.keys(), id_quantity_line.values()):
        line_values = dict(sorted(line_values.items(), key=lambda x: x[1][0], reverse=True))
        line_values = dict(sorted(line_values.items(), key=lambda x: (x[1][2] not in IMPORTANT_CUSTOMERS_MUBEA, IMPORTANT_CUSTOMERS_MUBEA.index(x[1][2]) if x[1][2] in IMPORTANT_CUSTOMERS_MUBEA else float('inf'))))
        line_values = dict(sorted(line_values.items(), key=lambda x: (x[0] not in CRITICAL_PARTS, CRITICAL_PARTS.index(x[0]) if x[0] in CRITICAL_PARTS else float('inf'))))

        id_quantity_line[line] = line_values
    output_data = {}

    if PRINT:
        for i in production_per_day_per_ident.items():
            print(i)
    
    nday = N_DAYS
    if len(sys.argv) > 2:
        nday = int(sys.argv[2])

    line_day_capacity = {day: {} for day in production_per_day_per_ident.keys()}
    # Populate the ident_day_capacity dictionary
    for day in production_per_day_per_ident.keys():
        for line in line_capacity.keys():
            if day[1] in SHIFTS:
                line_day_capacity[day][line] = ceil(line_capacity[line] * SHIFTS[day[1]] / 3)
            else:
                line_day_capacity[day][line] = line_capacity[line]

    # Change day logic
    if len(sys.argv) > 1:
        day = list(production_per_day_per_ident.keys())[0]
        if day[1] != sys.argv[1]:
            values_to_add = production_per_day_per_ident[day].items()
            production_per_day_per_ident.pop(day)
        for day in list(production_per_day_per_ident.keys()):
            if values_to_add:
                for values in values_to_add:
                    production_per_day_per_ident[day][values[0]] = production_per_day_per_ident[day].get(values[0], 0) + values[1]
            if day[1] != sys.argv[1]:
                values_to_add = production_per_day_per_ident[day].items()
                production_per_day_per_ident.pop(day)
            elif day[1] == sys.argv[1]:
                break
            else:
                values_to_add = False
        # Remove or add days
        while len(list(production_per_day_per_ident.keys())) > nday:
            production_per_day_per_ident.pop(list(production_per_day_per_ident.keys())[-1])
        while len(list(production_per_day_per_ident.keys())) < nday:
            input_date = datetime.datetime.strptime(list(production_per_day_per_ident.keys())[-1][1], '%d.%m.%Y')
            next_day = input_date + datetime.timedelta(days=1)
            old_day_string = next_day.strftime('%d.%m.%Y')
            production_per_day_per_ident[(list(production_per_day_per_ident.keys())[-1][0] % 7 + 1, old_day_string)] = {}
        count = 0
        for key in production_per_day_per_ident.keys():
            if key[1] == sys.argv[1]:
                break
            count += 1

        new_first_day = (list(production_per_day_per_ident.keys())[0][0] + count - 1) % 7 + 1 
        old_last_day = list(production_per_day_per_ident.keys())[-1]
        first_day = sys.argv[1]
        for key in production_per_day_per_ident.keys():
            if key[1] == sys.argv[1]:
                break
            for k, v in zip(production_per_day_per_ident[key].keys(), production_per_day_per_ident[key].values()):
                production_per_day_per_ident[(new_first_day,sys.argv[1])][k] = production_per_day_per_ident[(new_first_day,sys.argv[1])].get(k , 0) + v
            
        keys_to_remove = list(production_per_day_per_ident.keys())[:count]

        for key in keys_to_remove:
            production_per_day_per_ident.pop(key)

        input_date = datetime.datetime.strptime(old_last_day[1], '%d.%m.%Y')
        for i in range(count):
            future_date = input_date + datetime.timedelta(days=i + 1)
            production_per_day_per_ident[((old_last_day[0] + i) % 7 + 1,future_date.strftime('%d.%m.%Y'))] = {}

        #pendant_day_capacity = {day: {} for day in production_per_day_per_ident.keys()}
        #for day in production_per_day_per_ident.keys():
        #    for pendant in pendant_capacity.keys():
        #        if day[1] in SHIFTS:
        #            pendant_day_capacity[day][pendant] = ceil(pendant_capacity[pendant] * SHIFTS[day[1]] / 3)
        #        else:
        #            pendant_day_capacity[day][pendant] = pendant_capacity[pendant]

        line_day_capacity = {day: {} for day in production_per_day_per_ident.keys()}
        for day in production_per_day_per_ident.keys():
            if day not in line_day_capacity:
                line_day_capacity[day] = {}
            for line in line_capacity.keys():
                if day[1] in SHIFTS:
                    line_day_capacity[day][line] = ceil(line_capacity[line] * SHIFTS[day[1]] / 3)
                else:
                    line_day_capacity[day][line] = line_capacity[line]




    if PRINT:
        for i in production_per_day_per_ident.items():
            print(i)
    # Substract delivery
    print('Substract delivery')
    
    for day, values in zip(production_per_day_per_ident.keys(), production_per_day_per_ident.values()):
        for ident, value in zip(values.keys(), values.values()):
            if ident in delivery_per_ident:
                substracted = delivery_per_ident[ident]
                if delivery_per_ident[ident] >= value:
                    delivery_per_ident[ident] -= value
                    production_per_ident[ident] -= value
                    production_per_day_per_ident[day][ident] -= value
                elif delivery_per_ident[ident] < value:
                    delivery_per_ident[ident] -= substracted
                    production_per_ident[ident] -= substracted
                    production_per_day_per_ident[day][ident] -= substracted
    
    if PRINT:
        for i in production_per_day_per_ident.items():
            print(i)
    '''
    # Split production
    
    day_list_w_sunday = list(production_per_day_per_ident.keys())
    day_list = [item for item in day_list_w_sunday if item[0] not in NO_WORK_DAYS]
    
    iter = 0
    print('Split production')
    for day, values in zip(production_per_day_per_ident.keys(), production_per_day_per_ident.values()):
        if day[0] not in NO_WORK_DAYS:
            iter += 1
        for value in values.items():
            new_value = production_per_day_per_ident[day][value[0]]
            iter_copy = iter if iter != 0 else 1
            for pendant_values in id_quantity_pendant.values():
                if value[0] in pendant_values:
                    multiplier = pendant_values[value[0]][0] * GB_MULTIPLIER if pendant_values[value[0]][0] != '' else 1
                    if SAME_MULTIPLIER:
                        multiplier = MULTIPLIER
                    break
            if new_value // iter_copy < multiplier // 2:
                while new_value // iter_copy < multiplier // 2:
                    if iter_copy == 1:
                        break
                    iter_copy -= 1

                for append_day in day_list[:iter_copy]:
                        production_per_day_per_ident[append_day][value[0]] = production_per_day_per_ident[append_day].get(value[0], 0) + new_value // iter_copy
            else:
                production_per_day_per_ident[day][value[0]] = 0
                for append_day in day_list[:iter]:
                    production_per_day_per_ident[append_day][value[0]] = production_per_day_per_ident[append_day].get(value[0], 0) + new_value // iter
    
    if PRINT:
        for i in production_per_day_per_ident.items():
            print(i)
    
    '''
    print('1st itetation')
    '''
    # 1st iteration according to Mubea's plan
    for day in tqdm(production_per_day_per_ident.keys(), colour='cyan'):
        output_data[day] = {}
        if day not in output_data:
            output_data[day] = {}
        #for pendant, pendant_values in zip(id_quantity_pendant.keys(), id_quantity_pendant.values()):
        for line, line_values in zip(id_quantity_line.keys(), id_quantity_line.values()):
            if line not in line_day_capacity[day]:
                continue
            for ident in line_values.keys():
                if ident in production_per_day_per_ident[day] and production_per_ident[ident] > 0:
                    multiplier = line_values[ident][0] * GB_MULTIPLIER
                    if SAME_MULTIPLIER:
                        multiplier = MULTIPLIER
                    if day[0] in NO_WORK_DAYS:
                        backlog[ident] += production_per_day_per_ident[day][ident]
                        continue
                    if supply_per_ident[ident] <= 0:
                        backlog[ident] += production_per_day_per_ident[day][ident]
                        continue
                    if multiplier != '1':
                        added = multiplier
                        while added + multiplier <= min(production_per_day_per_ident[day][ident], line_day_capacity[day][line], supply_per_ident[ident]) and added + multiplier <= min(production_per_day_per_ident[day][ident], line_day_capacity[day][line], supply_per_ident[ident]):
                            added += multiplier
                    else:
                        added = production_per_day_per_ident[day][ident]

                    if added >= min(line_day_capacity[day][line], supply_per_ident[ident]):
                        added = added // 2
                        if added >= min(line_day_capacity[day][line], supply_per_ident[ident]):
                            backlog[ident] += production_per_day_per_ident[day][ident]
                            continue
                    if added < 100:
                        backlog[ident] += production_per_day_per_ident[day][ident]
                        continue
                    if added > min(supply_per_ident[ident],production_per_day_per_ident[day][ident]):
                        backlog[ident] += production_per_day_per_ident[day][ident]
                        continue

                    supply_per_ident[ident] -= added
                    production_per_day_per_ident[day][ident] -= added
                    line_day_capacity[day][line] -= added
                    output_data[day][(ident, line)] = output_data[day].get((ident, line), 0) + added
    '''
    for day, idents in zip(production_per_day_per_ident.keys(), production_per_day_per_ident.values()):
        if day not in output_data:
            output_data[day] = {}

        for ident in idents.keys():
            backlog[ident] += production_per_day_per_ident[day][ident]

    line_capacity_copy = line_capacity.copy()
    for line, line_values in zip(id_quantity_line.keys(), id_quantity_line.values()):
        if line not in line_capacity_copy:
                continue
        for ident in line_values.keys():
            if ident in backlog:
                if backlog[ident] > 0:
                    #print(ident, supply_per_ident[ident], backlog[ident])
                    multiplier = line_values[ident][0] * GB_MULTIPLIER
                    supply_per_ident[ident] = ceil(supply_per_ident.get(ident, 0) / multiplier) * multiplier
                    backlog[ident] = ceil(backlog.get(ident, 0) / multiplier) * multiplier
                    #print(ident, supply_per_ident[ident], backlog[ident], multiplier)


    # 2nd iteration according to backlog
    line_day_capacity_copy = copy.deepcopy(line_day_capacity)
    print('2nd iteration')
    for day in tqdm(output_data.keys(), colour='cyan'):
        line_capacity_copy = copy.deepcopy(line_capacity)
        limiter = copy.deepcopy(LIMITER)
        #for pendant, pendant_values in zip(id_quantity_pendant.keys(), id_quantity_pendant.values()):
        for line, line_values in zip(id_quantity_line.keys(), id_quantity_line.values()):
            if line not in line_capacity_copy:
                continue
            for ident in line_values.keys():
                if ident in backlog:
                    if backlog[ident] > 0:
                        f_index = False
                        for i in range(len(limiter)):
                            if ident in limiter[i][0]:
                                f_index = i
                                break
                        
                        multiplier = line_values[ident][0] * GB_MULTIPLIER
                        half_max_cap = ceil(line_day_capacity_copy[day][line] / 2 / multiplier) * multiplier
                        half_max_lim = ceil(LIMITER[f_index][1] // 2 / 2 / multiplier) * multiplier
                        if SAME_MULTIPLIER:
                            multiplier = MULTIPLIER
                        delivery_day = line_values[ident][1]
                        if day[0] == delivery_day or day[0] in NO_WORK_DAYS:
                            continue
                        if f_index is False:
                            if supply_per_ident[ident] <= 0 or line_day_capacity[day][line] <= 0:
                                continue
                        else:
                            if supply_per_ident[ident] <= 0 or limiter[f_index][1] <= 0 or line_day_capacity[day][line] <= 0:
                                continue
                        if multiplier != '1':
                            added = multiplier
                            if f_index is False:
                                #if added >= min(line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                #    added = added // 2
                                while added + multiplier <= min(half_max_cap, line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                    added += multiplier
                            else:
                                #if added >= min(line_day_capacity[day][line], KSB_vorne_cap[day], backlog[ident], supply_per_ident[ident]):
                                #    added = added // 2
                                while added + multiplier <= min(half_max_cap, line_day_capacity[day][line], limiter[f_index][1], half_max_lim, backlog[ident], supply_per_ident[ident]):
                                    added += multiplier
                        
                        if f_index is False:
                            if added > min(half_max_cap, line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                continue
                        else:
                            if added > min(half_max_cap, line_day_capacity[day][line], limiter[f_index][1], half_max_lim, backlog[ident], supply_per_ident[ident]):
                                continue
                        if added <= 100:
                            continue
                        if added > supply_per_ident[ident]:
                            continue

                        backlog[ident] -= added
                        supply_per_ident[ident] -= added
                        if f_index is False:
                            line_day_capacity[day][line] -= added
                        else:
                            line_day_capacity[day][line] -= added
                            limiter[f_index][1] -= added

                        output_data[day][(ident, line)] = output_data[day].get((ident, line), 0) + added

                    added = 0

        #for pendant, pendant_values in zip(id_quantity_pendant.keys(), id_quantity_pendant.values()):
        for line, line_values in zip(id_quantity_line.keys(), id_quantity_line.values()):
            if line not in line_capacity_copy:
                continue
            for ident in line_values.keys():
                if ident in backlog:
                    if backlog[ident] > 0:
                        f_index = False
                        for i in range(len(limiter)):
                            if ident in limiter[i][0]:
                                f_index = i
                                break
                        
                        multiplier = line_values[ident][0] * GB_MULTIPLIER
                        if SAME_MULTIPLIER:
                            multiplier = MULTIPLIER
                        delivery_day = line_values[ident][1]
                        if day[0] == delivery_day or day[0] in NO_WORK_DAYS:
                            continue
                        if f_index is False:
                            if supply_per_ident[ident] <= 0 or line_day_capacity[day][line] <= 0:
                                continue
                        else:
                            if supply_per_ident[ident] <= 0 or limiter[f_index][1] <= 0 or line_day_capacity[day][line] <= 0:
                                continue
                        if multiplier != '1':
                            added = multiplier
                            if f_index is False:
                                #if added >= min(line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                #    added = added // 2
                                while added + multiplier <= min(line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                    added += multiplier
                            else:
                                #if added >= min(line_day_capacity[day][line], KSB_vorne_cap[day], backlog[ident], supply_per_ident[ident]):
                                #    added = added // 2
                                while added + multiplier <= min(line_day_capacity[day][line], limiter[f_index][1], backlog[ident], supply_per_ident[ident]):
                                    added += multiplier
                       
                        if f_index is False:
                            if added > min(line_day_capacity[day][line], backlog[ident], supply_per_ident[ident]):
                                continue
                        else:
                            if added > min(line_day_capacity[day][line], limiter[f_index][1], backlog[ident], supply_per_ident[ident]):
                                continue
                        if added <= 100:
                            continue
                        if added > supply_per_ident[ident]:
                            continue

                        backlog[ident] -= added
                        supply_per_ident[ident] -= added
                        if f_index is False:
                            line_day_capacity[day][line] -= added
                        else:
                            line_day_capacity[day][line] -= added
                            limiter[f_index][1] -= added

                        output_data[day][(ident, line)] = output_data[day].get((ident, line), 0) + added

                    added = 0
    # 3rd iteration according to backlog
        

    if PRINT:
        for i in output_data.items():
            print(i)


    save_data_xlsx(output_data, first_day, delivery_per_day_per_ident_copy, backlog_copy, supply_copy, name, production_per_ident, produced_cw, produced_type_day)
    

def save_data_xlsx(data, first_day, delivery_per_day_per_ident, backlog, supply, name, production_per_ident, produced_cw, produced_type_day):
    
    # offset of a character
    def oc(letter, offset):
        alphabet = string.ascii_uppercase
        index = alphabet.index(letter)
        new_index = (index + offset) % len(alphabet)
        new_letter = alphabet[new_index]
        # Special case to handle moving to two characters after 'Z'
        if offset > 25:
            return letter + alphabet[new_index-1]
        else:
            return new_letter
        
    excel_app = open_excel_app()
    sheet = open_worksheet(OUTPUT_PRODUCTION_PLAN_TABLE_FILE, excel_app, sheet='Wygenerowany')
    print(f"Saving to {OUTPUT_PRODUCTION_PLAN_TABLE_FILE}")
    
    # Clear values
    sheet.Cells.ClearContents()
    # Clear formats and styles
    sheet.Cells.ClearFormats()

    sheet.Range('1:12').EntireRow.Hidden = True

    delv_days = len(delivery_per_day_per_ident)
    n_days = len(data.keys())

    current_values = []
    
    sheet.Cells(1, 2).Value = 'ile zawieszek jest na linii L1 i L7 razem'

    sheet.Cells(2, 4).Value = 'typ zawieszki'

   
    # Checkbox initial value + color white to hide
    sheet.Cells(3, 1).Formula = '=TRUE'
    sheet.Cells(3, 1).Font.ColorIndex = 2

    sheet.Cells(4, 1).Formula = '=TRUE'
    sheet.Cells(4, 1).Font.ColorIndex = 2

    col = 0
    row = 0

    ident_row_index = {}
    print('Iterating over days.')
    for day in tqdm(data.keys(), colour='cyan'):
        # dni
        sheet.Cells(13, 6 + col).Value = day[1]
        sheet.Cells(13, 6 + col).NumberFormat = "dd.mm.rrrr"

        sheet.Cells(13, 6 + col).Interior.Color = TABLE_COLOUR
        sheet.Cells(14, 6 + col).Formula = f'=TEXT({oc("F", col)}13, "dddd")'
        sheet.Columns(f'{oc("D", col)}').ColumnWidth = 13.57
        # zawieszki
        sheet.Cells(2, 5 + col).Formula = f'={oc("E", col)}13'
        sheet.Cells(2 ,5 + col).Interior.Color = TABLE_COLOUR
        for k in range(3,12):
            if day == list(data.keys())[0]:
                sheet.Cells(k, 2).Formula = f'=IF(AND($A$3, $A$4),Zawieszki!C{k-1}, IF(OR($A$3, $A$4), Zawieszki!C{k-1}/2, 0))'
                sheet.Cells(k, 2).NumberFormat = "0"
                sheet.Cells(k, 4).Formula = f'=Zawieszki!A{k-1}'
            sheet.Cells(k, 5 + col).Formula = f'=SUMIF($D$15:$D$1162,Zawieszki!A{k-1},{oc("E", col)}$15:{oc("E", col)}$1162)/Zawieszki!B{k-1}/(24/2.75)'
            sheet.Cells(k, 5 + col).NumberFormat = "0"
        for values in data[day].items():
            if values[0][0] not in ident_row_index:
                ident_row_index[values[0][0]] = 15 + row
                if values not in current_values:
                    current_values.append(values[0][0])

                sheet.Cells(15 + row, 1).Value = values[0][0]
                sheet.Cells(15 + row, 2).Value = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:I, 9, FALSE)"
                sheet.Cells(15 + row, 3).Value = name[values[0][0]]
                sheet.Cells(15 + row, 4).Value =f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:H, 8, FALSE)"
                sheet.Cells(15 + row, 5).Value = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:E, 5, FALSE)"
                sheet.Cells(15 + row, 6 + col).Value = values[1]
                sheet.Cells(15 + row, 6 + n_days).Formula = f'=SUM(F{15+row}:{oc("A",4+n_days)}{15+row})'
                sheet.Cells(15 + row, 7 + n_days).Value = produced_cw[values[0][0]] if values[0][0] in produced_cw else 0
                sheet.Cells(15 + row, 8 + n_days).Formula = f'=SUM({oc("A", 12+n_days)}{15 + row}:{oc("A", delv_days+12+n_days)}{15 + row})'
                sheet.Cells(15 + row, 9 + n_days).Value = production_per_ident.get(values[0][0], 0)
                sheet.Cells(15 + row, 10 + n_days).Value = backlog[values[0][0]]
                sheet.Cells(15 + row, 11 + n_days).Value = supply[values[0][0]]
                sheet.Cells(15 + row, 12 + n_days).Formula = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:B, 2, FALSE)"
                row += 1
            else:
                sheet.Cells(ident_row_index[values[0][0]], 6 + col).Value = values[1]
        col += 1

    # Coloring & formating
    # Main Table
    heading_range_main = sheet.Range(
        sheet.Cells(14, 1),
        sheet.Cells(14, 12 + n_days)
    )
    heading_range_main.Interior.Color = TABLE_COLOUR
    heading_range_main.Font.Size = 14
    heading_range_main.Font.Bold = True
    heading_range_main.Borders.LineStyle = win32.constants.xlContinuous
    heading_range_main.AutoFilter(1)

    heading_range_lower = sheet.Range(
        sheet.Cells(13, 5),
        sheet.Cells(13, 5 + n_days)
    )
    heading_range_lower.Font.Size = 14
    heading_range_lower.Font.Bold = True
    heading_range_lower.Borders.LineStyle = win32.constants.xlContinuous
    # Upper calculations
    pendant_range = sheet.Range(
        sheet.Cells(1, 2),
        sheet.Cells(11, 2)
    )
    pendant_range.Borders.LineStyle = win32.constants.xlContinuous
    pendant_range.Font.Size = 11
    header_calculations_range = sheet.Range(
        sheet.Cells(2, 4),
        sheet.Cells(2, 4 + n_days)
    )
    
    calculations_range = sheet.Range(
        sheet.Cells(2, 4),
        sheet.Cells(11, 5 + n_days)
    )
    calculations_range.Borders.LineStyle = win32.constants.xlContinuous
    calculations_range.Font.Size = 11

    header_calculations_range.Font.Bold = True
    header_calculations_range.Font.Size = 14
    sheet.Cells(2, 4).Font.Bold = False
    sheet.Cells(2, 4).Font.Size = 11
    # Side columns
    sheet.Cells(14, 1).Value = 'ident'
    sheet.Columns(1).ColumnWidth = 10.71

    sheet.Cells(14, 2).Value = ' '
    sheet.Columns(2).ColumnWidth = 18.57

    sheet.Cells(14, 3).Value = 'nazwa'
    sheet.Columns(3).ColumnWidth = 33.86

    sheet.Cells(14, 4).Value = 'linia'
    sheet.Columns(4).ColumnWidth = 12

    sheet.Cells(14, 5).Value = 'zawieszka'
    sheet.Columns(5).ColumnWidth = 14.43
    
    sheet.Cells(14, 6 + n_days).Value = 'Do realizacji'
    sheet.Columns(6 + n_days).ColumnWidth = 16.43
    sheet.Cells(14, 6 + n_days).Interior.ColorIndex = 3

    sheet.Cells(14, 7 + n_days).Value = 'Wyprodukowane'
    sheet.Columns(7 + n_days).ColumnWidth = 23.43
    sheet.Cells(14, 7 + n_days).Interior.ColorIndex = 6
    KW_range = sheet.Range(
        sheet.Cells(13, 7 + n_days),
        sheet.Cells(13, 8 + n_days)
    )
    sheet.Cells(13, 7 + n_days).Value = f'KW{datetime.datetime.now().isocalendar()[1]}' if PRODUCTION_CW == '' else 'KW' + PRODUCTION_CW
    sheet.Cells(13, 7 + n_days).Font.Bold = True
    sheet.Cells(13, 7 + n_days).Font.Size = 14
    sheet.Cells(13, 7 + n_days).Interior.Color = TABLE_COLOUR
    KW_range.Merge()
    KW_range.WrapText = True
    KW_range.HorizontalAlignment = win32.constants.xlCenter
    KW_range.VerticalAlignment = win32.constants.xlCenter
    KW_range.Borders.LineStyle = win32.constants.xlContinuous

    sheet.Cells(14, 8 + n_days).Value = 'Wysłane'
    sheet.Columns(8 + n_days).ColumnWidth = 12.83
    sheet.Cells(14, 8 + n_days).Interior.ColorIndex = 33

    sheet.Cells(14, 9 + n_days).Value = 'Plan Mubea'
    sheet.Columns(9 + n_days).ColumnWidth = 17.14

    sheet.Cells(14, 10 + n_days).Value = 'zaległości'
    sheet.Columns(10 + n_days).ColumnWidth = 13.86
    sheet.Cells(14, 10 + n_days).Interior.Color = TABLE_COLOUR

    sheet.Cells(14, 11 + n_days).Value = 'Stan magazynowy'
    sheet.Columns(11 + n_days).ColumnWidth = 24.29  
    sheet.Cells(14, 11 + n_days).Interior.ColorIndex = 6

    sheet.Cells(14, 12 + n_days).Value = 'Ile w GB'
    sheet.Columns(12 + n_days).ColumnWidth = 12.29
    sheet.Cells(14, 12 + n_days).Interior.Color = TABLE_COLOUR

    sheet.Cells(13, 13 + n_days).Value = 'Wysyłki (dni)'
    sheet.Columns(13 + n_days).ColumnWidth = 15.57
    sheet.Cells(13, 13 + n_days).Font.Size = 14
    sheet.Cells(13, 13 + n_days).Interior.ColorIndex = 33
    sheet.Cells(13, 13 + n_days).Font.Bold = True
    sheet.Cells(13, 13 + n_days).Borders.LineStyle = win32.constants.xlContinuous

    for i in tqdm(range(13 + n_days, 13 + n_days + delv_days), colour='cyan'):
        sheet.Columns(i).ColumnWidth = 15.57
        sheet.Cells(14, i).Interior.ColorIndex = 34
        sheet.Cells(14, i).Font.Size = 14
        sheet.Cells(14, i).Font.Bold = True
        sheet.Cells(14, i).Borders.LineStyle = win32.constants.xlContinuous

    # conditon formmat
    calculations_range = sheet.Range(
        sheet.Cells(4, 5),
        sheet.Cells(11, 5 + n_days)
    )
    format_condition_1 = heading_range_main.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"=A14=\"sobota\"")
    format_condition_1.Interior.ColorIndex = 46


    format_condition_2 = heading_range_main.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"=A14=\"niedziela\"")
    format_condition_2.Interior.ColorIndex = 46
    

    format_condition_3 = calculations_range.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"=E3>$B3")
    format_condition_3.Interior.ColorIndex = 3
    old_row = row
    for line in range(1, 3):
        sheet.Cells(15 + row, 4).Value = line
        sheet.Cells(15 + row, 4).Interior.Color = TABLE_COLOUR
        sheet.Cells(15 + row, 4).Font.Size = 14
        sheet.Cells(15 + row, 4).Font.Bold = True
        sheet.Cells(15 + row, 4).Borders.LineStyle = win32.constants.xlContinuous
        for i in range(n_days):
            sheet.Cells(15 + row, 6 + i).Formula = f"=SUMIFS({oc('F', i)}15:{oc('F', i)}{14+old_row},D15:D{14+old_row},{line})"
        row += 1

    sheet.Cells(15 + row, 2).Value = 'Stange PLAN'
    sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 2).Font.Size = 14
    sheet.Cells(15 + row, 2).Font.Bold = True
    sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
    
    for i in range(n_days):
        sheet.Cells(15 + row, 6 + i).Formula = f"=SUMIFS({oc('F', i)}15:{oc('F', i)}{14+old_row},B15:B{14+old_row},\"=Stange\")"
    row += 1

    sheet.Cells(15 + row, 2).Value = 'Bügel   PLAN'
    sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 2).Font.Size = 14
    sheet.Cells(15 + row, 2).Font.Bold = True
    sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
    
    for i in range(n_days):
        sheet.Cells(15 + row, 6 + i).Formula = f"=({oc('F', i)}{12 + row}+{oc('F', i)}{13 + row})-{oc('F', i)}{14 + row}"
    row += 1

    sheet.Cells(15 + row, 2).Value = 'Stange PLAN %'
    sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 2).Font.Size = 14
    sheet.Cells(15 + row, 2).Font.Bold = True
    sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
    
    for i in range(n_days):
        sheet.Cells(15 + row, 6 + i).Formula = f'=IF(OR({oc("F", i)}14="poniedziałek",{oc("F", i)}14="sobota"),{oc("F", i)}{13+row}/50000,{oc("F", i)}{13+row}/75000)'
        sheet.Cells(15 + row, 6 + i).NumberFormat = "0,00%"

    sheet.Cells(15 + row, 6 + n_days).Formula = f"=SUM(F{15 + row}:{oc('F', n_days-1)}{15 + row})/COUNTIF(F14:{oc('F', n_days-1)}14,\"<>niedziela\")"
    sheet.Cells(15 + row, 6 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 6 + n_days).Font.Size = 15
    sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).NumberFormat = "0,00%"

    row += 1

    sheet.Cells(15 + row, 2).Value = 'Bügel   PLAN %'
    sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 2).Font.Size = 14
    sheet.Cells(15 + row, 2).Font.Bold = True
    sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
    
    for i in range(n_days):
        sheet.Cells(15 + row, 6 + i).Formula = f"=IF(OR({oc('F', i)}14=\"poniedziałek\",{oc('F', i)}14=\"sobota\"),{oc('F', i)}{13+row}/5000,{oc('F', i)}{13    +row}/12000)"
        sheet.Cells(15 + row, 6 + i).NumberFormat = "0,00%"
    
    sheet.Cells(15 + row, 6 + n_days).Formula = f"=SUM(F{15 + row}:{oc('F', n_days-1)}{15 + row})/COUNTIF(F14:{oc('F', n_days-1)}14,\"<>niedziela\")"
    sheet.Cells(15 + row, 6 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 6 + n_days).Font.Size = 15
    sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).NumberFormat = "0,00%"

    row += 1

    sum_range = sheet.Range(
        sheet.Cells(9 + row, 6),
        sheet.Cells(14 + row, 5 + n_days)
    )
    sum_range.Interior.Color = TABLE_COLOUR
    sum_range.Font.Size = 14
    sum_range.Font.Bold = True
    sum_range.Borders.LineStyle = win32.constants.xlContinuous
    sum_range.RowHeight = 19.5

    sheet.Cells(15 + row, 2).Formula = "."
    sheet.Cells(15 + row, 2).Font.ColorIndex = 2
    sheet.Cells(15 + row, 5 + n_days).Formula = "TOTAL"
    sheet.Rows(15 + row).RowHeight = 21
    sheet.Cells(15 + row, 5 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 5 + n_days).Font.Size = 16
    sheet.Cells(15 + row, 5 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 5 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).Formula = f"=({oc('F', n_days)}{13+row}+{oc('F', n_days)}{14+row})/2"
    sheet.Cells(15 + row, 6 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 6 + n_days).Font.Size = 16
    sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).NumberFormat = "0,00%"

    row += 1
    
    for s_type, days in zip(produced_type_day.keys(), produced_type_day.values()):
        sheet.Cells(15 + row, 2).Value = s_type
        sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
        sheet.Cells(15 + row, 2).Font.Size = 14
        sheet.Cells(15 + row, 2).Font.Bold = True
        sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous

        s_row = sheet.Columns(2).Find(s_type).Row
        for date, val in zip(days.keys(), days.values()):
            try:
                day_col = sheet.Rows(13).Find(date).Column
            except AttributeError:
                date = datetime.datetime.strptime(date, '%d.%m.%Y')
                day_col = sheet.Rows(13).Find(date).Column

            sheet.Cells(s_row, day_col).Value = val

        row += 1

    for s_type, days in zip(produced_type_day.keys(), produced_type_day.values()):
        sheet.Cells(15 + row, 2).Value = s_type + ' %'
        sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
        sheet.Cells(15 + row, 2).Font.Size = 14
        sheet.Cells(15 + row, 2).Font.Bold = True
        sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
        for i in range(n_days):
            if 'Bügel' in s_type:
                sheet.Cells(15 + row, 6 + i).Formula = f"=IF(OR({oc('F', i)}14=\"poniedziałek\",{oc('F', i)}14=\"sobota\"),{oc('F', i)}{13+row}/5000,{oc('F', i)}{13+row}/12000)"
            elif 'Stange' in s_type:
                sheet.Cells(15 + row, 6 + i).Formula = f'=IF(OR({oc("F", i)}14="poniedziałek",{oc("F", i)}14="sobota"),{oc("F", i)}{13+row}/50000,{oc("F", i)}{13+row}/75000)'
        
            sheet.Cells(15 + row, 6 + i).NumberFormat = "0,00%"

        sheet.Cells(15 + row, 6 + n_days).Formula = f"=SUM(F{15 + row}:{oc('F', n_days-1)}{15 + row})/COUNTIF(F14:{oc('F', n_days-1)}14,\"<>niedziela\")"
        sheet.Cells(15 + row, 6 + n_days).Interior.Color = ROW_COLOUR
        sheet.Cells(15 + row, 6 + n_days).Font.Size = 15
        sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
        sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous
        sheet.Cells(15 + row, 6 + n_days).NumberFormat = "0,00%"

        row += 1

    sum_range = sheet.Range(
        sheet.Cells(11 + row, 6),
        sheet.Cells(14 + row, 5 + n_days)
    )

    sum_range.Interior.Color = TABLE_COLOUR
    sum_range.Font.Size = 14
    sum_range.Font.Bold = True
    sum_range.Borders.LineStyle = win32.constants.xlContinuous
    sum_range.RowHeight = 19.5
        
    sheet.Cells(15 + row, 5 + n_days).Value = "TOTAL"
    sheet.Rows(15 + row).RowHeight = 21
    sheet.Cells(15 + row, 5 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 5 + n_days).Font.Size = 16
    sheet.Cells(15 + row, 5 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 5 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).Formula = f"=({oc('F', n_days)}{13+row}+{oc('F', n_days)}{14+row})/2"
    sheet.Cells(15 + row, 6 + n_days).Interior.Color = ROW_COLOUR
    sheet.Cells(15 + row, 6 + n_days).Font.Size = 16
    sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 6 + n_days).NumberFormat = "0,00%"

    row += 3

    sheet.Cells(15 + row, 2).Value = "RÓŻNICA (PRODUKCJA - PLAN)"
    sheet.Cells(15 + row, 2).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 3).Interior.Color = TABLE_COLOUR
    sheet.Cells(15 + row, 2).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 3).Borders.LineStyle = win32.constants.xlContinuous
    sheet.Cells(15 + row, 2).Font.Size = 14
    sheet.Cells(15 + row, 2).Font.Bold = True

    for i in range(n_days):
        sheet.Cells(15 + row, 6 + i).Formula = f"=({oc('F', i)}{8 + row}+{oc('F', i)}{9 + row})- ({oc('F', i)}{4 + row}+{oc('F', i)}{3 + row})"
        sheet.Cells(15 + row, 6 + i).Interior.Color = TABLE_COLOUR
        sheet.Cells(15 + row, 6 + i).Borders.LineStyle = win32.constants.xlContinuous
        sheet.Cells(15 + row, 6 + i).Font.Size = 14
        sheet.Cells(15 + row, 6 + i).Font.Bold = True

    sheet.Cells(15 + row, 6 + n_days).Formula = f"=SUM(F{15 + row}:{oc('F', n_days-1)}{15 + row})"
    sheet.Cells(15 + row, 6 + n_days).Font.Size = 15
    sheet.Cells(15 + row, 6 + n_days).Font.Bold = True
    sheet.Cells(15 + row, 6 + n_days).Borders.LineStyle = win32.constants.xlContinuous



    diff_range = sheet.Range(
        sheet.Cells(15 + row, 6),
        sheet.Cells(15 + row, 6 + n_days)
    )
    format_condition_2 = diff_range.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"=F{15+row}>=0")
    format_condition_2.Interior.ColorIndex = 8
    

    format_condition_3 = diff_range.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"=F{15+row}<0")
    format_condition_3.Interior.ColorIndex = 3

    row += 2

    for values in tqdm(backlog.keys(), colour='cyan'):
        if values not in current_values:
            if values not in ident_row_index:
                ident_row_index[values] = 15 + row
            sheet.Cells(15 + row, 1).Value = values
            sheet.Cells(15 + row, 2).Value = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:I, 9, FALSE)"
            sheet.Cells(15 + row, 3).Value = name[values]
            sheet.Cells(15 + row, 4).Value = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:H, 8, FALSE)"
            sheet.Cells(15 + row, 5).Value = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:E, 5, FALSE)"
            sheet.Cells(15 + row, 7 + n_days).Value = produced_cw[values] if values in produced_cw else 0
            sheet.Cells(15 + row, 8 + n_days).Formula = f'=SUM({oc("A", 12+n_days)}{15 + row}:{oc("A", delv_days+12+n_days)}{15 + row})'
            sheet.Cells(15 + row, 9 + n_days).Value = production_per_ident.get(values, 0)
            sheet.Cells(15 + row, 10 + n_days).Value = backlog[values]
            sheet.Cells(15 + row, 11 + n_days).Value = supply[values]
            sheet.Cells(15 + row, 12 + n_days).Formula = f"=VLOOKUP(A{15+row}, 'Dostawcy Mubea'!A:B, 2, FALSE)"
            row += 1

    # Add delivery day
    for index, day in tqdm(enumerate(list(delivery_per_day_per_ident.keys()), start=1), colour='cyan'):
        sheet.Cells(14, index + 12 + n_days).Value = day
        for values in delivery_per_day_per_ident[day].items():
            if values[0] in ident_row_index: 
                sheet.Cells(ident_row_index[values[0]], index + 12 + n_days).Value = values[1]

    # Sorting main plan
    sort_range_1 = sheet.Range(
        sheet.Cells(15, 1),
        sheet.Cells(14 + old_row, 12 + n_days + delv_days)
    )
    sort_range_1.Sort(Key1=sort_range_1.Columns(1), Order1=1, Orientation=1)

    for i in tqdm(range(15, 15 + old_row), colour='cyan'):
        color = ROW_COLOUR if i % 2 == 0 else 0xffffff
        color_range = sheet.Range(f'A{i}:{oc("A", 11+n_days+delv_days)}{i}')
        color_range.Interior.Color = color
        color_range.Font.Size = 12
        color_range.Font.Bold = True
        color_range.Borders.LineStyle = win32.constants.xlContinuous

    sort_range_2 = sheet.Range(
        sheet.Cells(31 + old_row, 1),
        sheet.Cells(15 + row, 12 + n_days + delv_days)
    )
    sort_range_2.Sort(Key1=sort_range_2.Columns(17), Order1=2, Orientation=1)
    
    for i in tqdm(range(31 + old_row, 15 + row), colour='cyan'):
        color = ROW_COLOUR if i % 2 == 0 else 0xffffff
        color_range = sheet.Range(f'A{i}:{oc("A", 11+n_days+delv_days)}{i}')
        color_range.Interior.Color = color
        color_range.Font.Size = 12
        color_range.Font.Bold = True
        color_range.Borders.LineStyle = win32.constants.xlContinuous
    
    # Freeze the view
    sheet.Cells(15, 2).Select()
    excel_app.ActiveWindow.FreezePanes = True

if __name__ == "__main__":
    start_time = time.time()
    create_plan()
    end_time = time.time() - start_time
    print(f"Proces took: {end_time} seconds")
import pandas as pd
import requests
import win32com.client as win32
from config_GSheets import *
from tqdm import tqdm
from win32com.client import Dispatch


def rw_excel():
    raw_data = pd.read_excel(CURRENT_RAW_FILE, sheet_name='aktualny raw', usecols=[0, 6, 12], skiprows=range(1, 11))
    print(f'Wczytano {CURRENT_RAW_FILE}')

    spec_data = pd.read_excel(CURRENT_SPEC_FILE, sheet_name='nowa specyfikacja ', usecols=[0], skiprows=5)
    print(f'Wczytano {CURRENT_SPEC_FILE}')

    spec_data = spec_data.dropna()
    send_gtm_codes = spec_data.iloc[:, 0].values

    excel_app = Dispatch('Excel.Application')
    excel_app.Visible = True
    workbook = excel_app.Workbooks.Open(OUTPUT_FILE_PATH) 
    try:
        named_range = workbook.Names.Item("_FilterDatabase")
        named_range.Delete()
    except Exception as e:
        pass

    worksheet = workbook.Worksheets(1)
    
    # Clear columns A:D
    worksheet.Range('A:D').ClearContents()

    # Kontrola jakości
    q_control_dict = read_gsheets('quality_control.csv', GSHEET_QUALITY)
    print('Otwarto gsheet/Kontrola jakości')

    # Przyjecie na linie 7
    prod_dict = read_gsheets('production.csv', GSHEET_PRODUCTION)
    print('Otwarto gsheet/Przyjecie na linie 7')

    count = 1
    cell_1 = worksheet.Cells(count, 1)
    cell_2 = worksheet.Cells(count, 2)
    cell_3 = worksheet.Cells(count, 3)
    cell_4 = worksheet.Cells(count, 4)

    cell_1.Value = 'nr karty'
    cell_2.Value = 'Ident'
    cell_3.Value = 'Ilość'
    cell_4.Value = 'Data Produkcji'

    worksheet.Columns(1).ColumnWidth = 9.29
    worksheet.Columns(2).ColumnWidth = 8.29
    worksheet.Columns(3).ColumnWidth = 6.57
    worksheet.Columns(4).ColumnWidth = 15.43

    # Saving to excel
    print('Zapisywanie do excel')
    for index, row in tqdm(raw_data.iterrows()):
        if pd.isna(row[1]):
            break
        
        if not isinstance(row[0], int):
            continue

        GTM_code = str(row[0])
        GTM_code_number = row[0]

        if GTM_code_number < SKIP_BELOW:
            continue

        ident = int(row[1])
        quantity = int(row[2])
        prod_date = prod_dict.get(GTM_code, 0)
        control_date = q_control_dict.get(GTM_code, '')

        if prod_date == 0:
            continue
        if control_date != '':
            continue
        if GTM_code_number in send_gtm_codes:
            continue
        
        if not pd.isna(prod_date):
            count += 1
            cell_1 = worksheet.Cells(count, 1)
            cell_2 = worksheet.Cells(count, 2)
            cell_3 = worksheet.Cells(count, 3)
            cell_4 = worksheet.Cells(count, 4)

            cell_1.Value = GTM_code
            cell_2.Value = ident
            cell_3.Value = quantity
            cell_4.Value = prod_date


def read_gsheets(file_name, sheet_id):
    r = requests.get(f'https://docs.google.com/spreadsheet/ccc?key={sheet_id}&output=csv')
    open(file_name, 'wb').write(r.content)
    df = pd.read_csv(file_name)

    result_dict = {}

    for index, row in df.iterrows():
        key = str(row[0])
        value = str(row[1])
    
        result_dict[key] = value

    return result_dict
    

if __name__ == "__main__":
    rw_excel()

import os
import csv


def get_latest_file(folder_path, start_str, end_str):
    files = os.listdir(folder_path)
    filtered_files = [file for file in files if file.startswith(start_str) and file.endswith(end_str)]
    filtered_files.sort(key=lambda x: os.path.getmtime(folder_path + '\\' + x), reverse=True)
    latest_file_path = folder_path + '\\' + filtered_files[0]

    return latest_file_path


def delete_files(files_list):
    for file in files_list:
        os.remove(file)
        print(f"\nDeleted {file}\n")


def open_excel_file(file_path, excel_app):
    workbook = excel_app.Workbooks.Open(file_path) 

    try:
        filter_db = workbook.Names.Item("_FilterDatabase")
        filter_db.Delete()
    except Exception as e:
        pass
    
    return workbook


def csv_reader(file_path, mode='r', encoding='utf-16', delimiter='\t'):
    with open(file_path, mode=mode, encoding=encoding) as file:
        reader = csv.reader(file, delimiter=delimiter)
        yield reader
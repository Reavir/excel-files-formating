import string

import pandas as pd
from win32com.client import Dispatch


def open_excel_app(visible=True):
    try:
        from win32com.client.gencache import EnsureDispatch
        excel_app = EnsureDispatch('Excel.Application')
    except TypeError:
        excel_app = Dispatch('Excel.Application')

    excel_app.Visible = visible

    return excel_app


def close_excel_app(workbook, excel_app, save=False, save_path=0):
    if save_path:
        workbook.SaveAs(save_path)
    
    workbook.Close(SaveChanges=save)
    excel_app.Quit()


def open_worksheet(file_path, excel_app, sheet=1):
    workbook = excel_app.Workbooks.Open(file_path)
    try:
        filter_db = workbook.Names.Item("_FilterDatabase")
        filter_db.Delete()
    except Exception as e:
        pass
    worksheet = workbook.Worksheets(sheet)

    return worksheet


def oc(letter, offset):
    alphabet = string.ascii_uppercase
    index = alphabet.index(letter)
    new_index = (index + offset) % len(alphabet)
    new_letter = alphabet[new_index]
    # Special case to handle moving to two characters after 'Z'
    if offset > 25:
        return letter + alphabet[new_index-1]
    else:
        return new_letter
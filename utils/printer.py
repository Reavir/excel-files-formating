# win32 print
def win32_print_pdf(pdf_file_path):
    import win32print
    

    hPrinter = win32print.OpenPrinter('NPI97265D (HP LaserJet P4015)')

    try:
        hJob = win32print.StartDocPrinter(hPrinter, 1, ("Custom Document", None, "RAW"))
        win32print.StartPagePrinter(hPrinter)

        # Send the PDF content to the printer
        with open(pdf_file_path, "rb") as pdf_file:
            pdf_content = pdf_file.read()
            win32print.WritePrinter(hPrinter, pdf_content)

        win32print.EndPagePrinter(hPrinter)
        win32print.EndDocPrinter(hPrinter)
    finally:
        win32print.ClosePrinter(hPrinter)


# os print
def os_print_pdf(pdf_file_path):
    import os

    try:
        os.startfile(pdf_file_path, "print")
    except OSError as e:
        print(f'Błąd drukowania: {e}')
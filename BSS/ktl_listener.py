from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from lxml import etree
import xml.etree.ElementTree as ET
from openpyxl import Workbook
from random import randint
import os
import time


DESKTOP_PATH = r'C:\Users\sysadm\Desktop'
XML_OUT = DESKTOP_PATH + r'\dane.xml'

def change_xls(xml_file_path):
    parser = etree.XMLParser(recover=True)
    tree = etree.parse(xml_file_path, parser)
    root = tree.getroot()

    tree.write(XML_OUT)

    tree = ET.parse(XML_OUT)
    root = tree.getroot()

    # Create a new Excel workbook and get the active sheet
    wb = Workbook()
    ws = wb.active

    # Write XML data to Excel sheet
    # Iterate through each Worksheet element in the XML
    for worksheet in root.findall('.//{urn:schemas-microsoft-com:office:spreadsheet}Worksheet'):
        # Extract the worksheet name
        worksheet_name = worksheet.attrib['{urn:schemas-microsoft-com:office:spreadsheet}Name']

        # Create a new worksheet in the Excel workbook
        ws = wb.create_sheet(title=worksheet_name)

        # Initialize row and column counters
        row_index = 1
        col_index = 1

        # Iterate through each Row element in the XML
        for row in worksheet.findall('.//{urn:schemas-microsoft-com:office:spreadsheet}Row'):
            # Reset column index for each new row
            col_index = 1

            # Iterate through each Cell element in the XML
            for cell in row.findall('.//{urn:schemas-microsoft-com:office:spreadsheet}Cell'):
                # Extract the cell value
                cell_value = cell.find('.//{urn:schemas-microsoft-com:office:spreadsheet}Data').text
                
                if worksheet_name == 'Czasy ekspozycji':
                    if row_index >= 2:
                        if col_index == 8:
                            max_val = int(cell_value)
                        elif col_index == 9:
                            if int(cell_value) > max_val:
                                cell_value = max_val 
                
                elif worksheet_name == 'Temperatura - przegląd':
                    if row_index >= 2:
                        if col_index == 3:
                            planend = float(cell_value)
                        elif col_index == 4:
                            avg_val = float(cell_value)
                        elif col_index == 5:
                            min_val = float(cell_value)
                        elif col_index == 6:
                            max_val = float(cell_value)
                        elif col_index == 7:
                            if max_val > planend:
                                max_val = planend
                                ws.cell(row=row_index, column=6, value="%.2f" %max_val)
                            if min_val > max_val:
                                min_val = max_val - randint(2, 5)
                                ws.cell(row=row_index, column=5, value="%.2f" %min_val)
                            
                            avg_val = (max_val + min_val) / 2
                            ws.cell(row=row_index, column=4, value="%.2f" % avg_val)

                # Write the cell value to the corresponding cell in the Excel worksheet
                if isinstance(cell_value, float):
                    ws.cell(row=row_index, column=col_index, value="%.2f" % cell_value)
                else:
                    cell_value = str(cell_value)
                    if cell_value != 'None':
                        ws.cell(row=row_index, column=col_index, value=str(cell_value))
                    else:
                        ws.cell(row=row_index, column=col_index, value='')

                # Increment column index
                col_index += 1

            # Increment row index
            row_index += 1
        
    # Save the Excel workbook
    ws_to_delete = wb['Sheet']
    # Remove the worksheet
    wb.remove(ws_to_delete)
    excel_file_path = xml_file_path
    wb.save(excel_file_path)
    print('Saved')
    os.remove(XML_OUT)
    print('Deleted')

class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
        print('created', event.src_path)
        if event.src_path.endswith(".xls") and '~' not in event.src_path:
            time.sleep(5)
            if '!UVNCPFT-' in event.src_path:
                new_path = event.src_path.replace('!UVNCPFT-', '')
            else:
                new_path = event.src_path
            
            print(new_path)

            try:
                change_xls(new_path)
            except Exception as e:
                print(e)
                pass


if __name__ == "__main__":    
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, DESKTOP_PATH, recursive=True)
    observer.start()

    try:
        while True:
            pass
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
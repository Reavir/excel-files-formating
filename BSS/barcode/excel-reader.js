
// Load the XLSX file
const fileInput = document.getElementById('fileInput'); // HTML input element for file upload

fileInput.addEventListener('change', (e) => {
    const file = e.target.files[0];

    if (file) {
        console.log("Starting to read...")
        const reader = new FileReader();

        reader.onprogress = (event) => {
            if (event.lengthComputable) {
                const percentLoaded = (event.loaded / event.total) * 100;
                fileProgress.value = percentLoaded;
            }
        };

        reader.onload = (e) => {
            fileProgress.value = 0;

            const data = e.target.result;
            const workbook = XLSX.read(data, { type: 'binary' });

            const sheetName = workbook.SheetNames[0];
            const sheet = workbook.Sheets[sheetName];

            const jsonData = XLSX.utils.sheet_to_json(sheet);
            const startRow = 7316;

            const userInput = parseInt(prompt("Enter a number:"), 10);
            const extractedNumbers = [];
            const extractedNames = [];
            const extractedQuantities = [];
            const extractedCards = [];

            fileProgress.value = 100;

            if (!isNaN(userInput)) { 
                jsonData.slice(startRow - 1).forEach((row) => {
                    if (
                        typeof row['order number'] === 'string' &&
                        row['order number'].includes('GTM') &&
                        typeof row['card…'] === 'string' &&
                        row['card…'].includes('poprawkowe')
                      ) {
                        const numberMatch = row['order number'].match(/\d+/)
                        if (numberMatch) {
                            const extractedNumber = parseInt(numberMatch[0], 10);
                            if (extractedNumber >= userInput) {
                                extractedNumbers.push(extractedNumber);
                                extractedNames.push(row[' ']);
                                extractedQuantities.push(row['Qty']);
                                extractedCards.push(row['card…']);
                                console.log(row);
                            }
                        }
                    }
                });

                localStorage.setItem('extractedNumbers', JSON.stringify(extractedNumbers));
                localStorage.setItem('extractedNames', JSON.stringify(extractedNames));
                localStorage.setItem('extractedQuantities', JSON.stringify(extractedQuantities));
                localStorage.setItem('extractedCards', JSON.stringify(extractedCards));
                console.log(extractedNames)
                console.log(extractedQuantities)
                console.log(extractedCards)
                openOrdersButton.href = 'barcode_4_4x6.html';
                openOrdersButton.textContent = 'Open Printing Layout';
                openOrdersButton.target = '_blank';

                const completeLoadingSpan = document.getElementById('completeLoading');
                completeLoadingSpan.textContent = 'Loading Complete';
                completeLoadingSpan.style.display = 'inline';

            } else {
                console.log("Invalid input. Please enter a valid number.");
              }

        };

        reader.readAsBinaryString(file);
    }
});

import sys
sys.path.insert(0, '..')
from utils.file_handler import get_latest_file, csv_reader
from utils.excel import *
from config import *

import win32com.client as win32
from datetime import datetime
import pandas as pd


def read_data():
    # Reading from odrers
    # FulfilmentDate    ArticleNumber    Name    Quantity   ExitDate    ProductionDate
    cols = [0, 2, 3, 4, 10, 12]
    raw_data = pd.read_excel(
        ORDERS_FILE,
        sheet_name='Zamówienia',
        usecols=cols,
        skiprows=range(2, 6208),
    )
    print(f'Wczytano {ORDERS_FILE}')
    raw_data['Date of order fulfillment'] = raw_data['Date of order fulfillment'].dt.strftime('%d.%m.%Y')
    production_data = pd.read_excel(
        ORDERS_FILE,
        sheet_name='Dane Produkcyjne',
    )
    print(f'Wczytano {ORDERS_FILE}')

    cart_capacity = {}
    for index, row in production_data.iterrows():
        if not pd.isna(row[0]):
            cart_capacity[row[0]] = row[2]

    production_per_article_per_day = {}
    quantity_per_article = {}
    for index, row in raw_data.iterrows():
        day = row[0]
        article_nr = row[1]
        article_name = row[2]
        quantity = row[3]
        if pd.isna(row[' order exit date']) and pd.isna(row['data produkcji ']) and not pd.isna(row['Date of order fulfillment']):
            if day not in production_per_article_per_day:
                production_per_article_per_day[day] = {}

            production_per_article_per_day[day][(article_nr, article_name)] = production_per_article_per_day[day].get((article_nr, article_name), 0) + quantity
            quantity_per_article[article_nr] = quantity

    production_per_article_per_day = {k: v for k, v in sorted(production_per_article_per_day.items(), key=lambda item: datetime.strptime(item[0], '%d.%m.%Y'))}

    # Reading from enova CSV
    file_path = get_latest_file(DESKTOP_PATH, 'Towary', '.csv')
    supply_data = {}
    for reader in csv_reader(file_path=file_path):
        print('Wczytano ' + DESKTOP_PATH + '\\' + 'Towary.csv')
        for row in reader:
            if len(row) == 13:
                supply_data[row[0]] = row[4]


    write_data(production_per_article_per_day, supply_data, quantity_per_article, cart_capacity)


def write_data(production_per_article_per_day, supply_data, quantity_per_article, cart_capacity):
    exl = open_excel_app()
    worksheet = open_worksheet(ORDERS_FILE, exl, sheet='Tabela Produkcyjna')
    # Clear values
    worksheet.Cells.ClearContents()
    # Clear formats and styles
    worksheet.Cells.ClearFormats()
    worksheet.Cells.ClearHyperlinks()
    # Constan name and formats
    worksheet.Cells(7, 1).Value = 'Nr'
    worksheet.Cells(7, 2).Value = 'Nazwa'

    worksheet.Cells(1, 2).Value = 'Dzień' 
    worksheet.Cells(2, 2).Value = 'Dzień tygodnia'
    worksheet.Cells(3, 2).Value = 'obciążenie min.'
    worksheet.Cells(4, 2).Value = 'obciążenie %'
    worksheet.Cells(5, 2).Value = 'ilość zjazdów'
    worksheet.Cells(6, 2).Value = 'ile kart KANBAN'

    worksheet.Columns(2).ColumnWidth = 26.57
    # Begin writing variables
    col = 3
    row = 8
    article_row_index = {}
    n_days = len(production_per_article_per_day.keys()) + 3
    for day, articles in zip(production_per_article_per_day.keys(), production_per_article_per_day.values()):
        # Day
        worksheet.Cells(1, col).Value = day
        worksheet.Cells(2, col).Formula = f'=TEXT({oc("A", col-1)}1, "dddd")'
        worksheet.Cells(2, col).Interior.Color = ROW_COLOUR

        worksheet.Cells(4, col).Formula = f'={oc("A", col-1)}3/1440'
        worksheet.Cells(4, col).NumberFormat = '0%'
        worksheet.Cells(4, col).Interior.Color = ROW_COLOUR
        # KANBAN
        worksheet.Cells(6, col).Interior.Color = ROW_COLOUR
        # Main row
        worksheet.Cells(7, col).Interior.Color = TABLE_COLOUR
        worksheet.Cells(7, col).Formula = f'=TEXT({oc("A", col-1)}1, "dd mmm")'
        worksheet.Cells(7, col).Font.Bold = True

        worksheet.Columns(col).ColumnWidth = 12.29
        for (article_nr, article_name), value in zip(articles.keys(), articles.values()):
            if article_nr not in article_row_index:
                article_row_index[article_nr] = row
                worksheet.Cells(row, 1).Value = article_nr
                worksheet.Cells(row, 2).Value = article_name
                capacity = cart_capacity[article_nr]
                quantity = quantity_per_article[article_nr]
                found = False
                if article_nr in ENOVA_MISSING.keys():
                    article_nr = ENOVA_MISSING[article_nr]
                
                for nr in supply_data.keys():
                    if str(article_nr) in nr and 'p' not in nr:
                        supply_value = supply_data[nr]
                        found = True
                    if not found:
                        supply_value = 0 

                worksheet.Cells(row, n_days).Formula = f'=SUM(C{row}:{oc("A", n_days-2)}{row})'
                worksheet.Cells(row, n_days+1).Value = supply_value
                worksheet.Cells(row, n_days+2).Formula = f'={oc("A", n_days)}{row}-{oc("A", n_days-1)}{row}'
                worksheet.Cells(row, n_days+3).Value = capacity
                worksheet.Cells(row, n_days+4).Value = quantity
                worksheet.Cells(row, col).Value = value
                row += 1
            else:
                worksheet.Cells(article_row_index[article_nr], col).Value = value

        col += 1
    
    # Borders
    upper_range = worksheet.Range(
        worksheet.Cells(1, 2),
        worksheet.Cells(6, col-1)
    )
    upper_range.Borders.LineStyle = win32.constants.xlContinuous

    lower_range = worksheet.Range(
        worksheet.Cells(7, 1),
        worksheet.Cells(row-1, col+4)
    )
    lower_range.Borders.LineStyle = win32.constants.xlContinuous
    # Coloring main range
    headers_range = worksheet.Range(
        worksheet.Cells(1, 1),
        worksheet.Cells(7, 2)
    )
    headers_range.Interior.Color = TABLE_COLOUR
    headers_range.Font.Bold = True

    worksheet.Cells(3, col+1).Value = 'OEE'
    worksheet.Cells(3, col+1).Interior.Color = TABLE_COLOUR
    worksheet.Cells(3, col+1).Font.Bold = True
    worksheet.Cells(3, col+1).Borders.LineStyle = win32.constants.xlContinuous
    worksheet.Cells(4, col+1).Value = OEE
    worksheet.Cells(4, col+1).NumberFormat = '0%'
    worksheet.Cells(4, col+1).Interior.Color = ROW_COLOUR
    worksheet.Cells(4, col+1).Font.Bold = True
    worksheet.Cells(4, col+1).Borders.LineStyle = win32.constants.xlContinuous
    
    worksheet.Cells(7, col).Value = 'Suma'
    worksheet.Cells(7, col+1).Value = 'Surówka'
    worksheet.Cells(7, col+2).Value = 'Różnica'
    worksheet.Cells(7, col+3).Value = 'Pojemność kosza'
    worksheet.Cells(7, col+4).Value = 'Ilość na kartę'

    worksheet.Cells(7, col).Interior.Color = TABLE_COLOUR
    worksheet.Cells(7, col).Font.Bold = True
    worksheet.Cells(7, col+1).Interior.Color = TABLE_COLOUR
    worksheet.Cells(7, col+1).Font.Bold = True
    worksheet.Cells(7, col+2).Interior.Color = TABLE_COLOUR
    worksheet.Cells(7, col+2).Font.Bold = True
    worksheet.Cells(7, col+3).Interior.Color = TABLE_COLOUR
    worksheet.Cells(7, col+3).Font.Bold = True
    worksheet.Columns(col+3).ColumnWidth = 15.86
    worksheet.Cells(7, col+4).Interior.Color = TABLE_COLOUR
    worksheet.Cells(7, col+4).Font.Bold = True
    worksheet.Columns(col+4).ColumnWidth = 15

    for index in range(3, n_days):
        worksheet.Cells(3, index).FormulaArray = f'=ZAOKR.GÓRA(SUMA(ZAOKR.GÓRA({oc("A", index-1)}8:{oc("A", index-1)}{row-1}/{oc("A", col+2)}8:{oc("A", col+2)}{row-1};0))*7/{oc("A", col)}{4};0)'
        worksheet.Cells(5, index).FormulaArray = f'=SUMA(ZAOKR.GÓRA({oc("A", index-1)}8:{oc("A", index-1)}{row-1}/{oc("A", col+2)}8:{oc("A", col+2)}{row-1};0))'
        worksheet.Cells(6, index).FormulaArray = f'=SUMA({oc("A", index-1)}8:{oc("A", index-1)}{row-1}/{oc("A", col+3)}8:{oc("A", col+3)}{row-1})'

    for index in range(8, row, 2):
        row_color_range = worksheet.Range(
            worksheet.Cells(index, 1),
            worksheet.Cells(index, col+4)
        )
        row_color_range.Interior.Color = ROW_COLOUR

    diff_range = worksheet.Range(
        worksheet.Cells(7, n_days+2),
        worksheet.Cells(row-1, n_days+2)
    )
    format_condition = diff_range.FormatConditions.Add(Type=win32.constants.xlExpression,
                                                         Formula1=f"={oc('A', col+1)}7<0")
    format_condition.Interior.ColorIndex = 3
    
    load_range = worksheet.Range(
        worksheet.Cells(3,  3),
        worksheet.Cells(3, col-1)
    )
    load_range.FormatConditions.AddDatabar()
    load_range.FormatConditions(load_range.FormatConditions.Count).ShowValue = True


if __name__ == "__main__":
    read_data()

import sys

sys.path.insert(0, '..')
import io
import os
import shutil
from datetime import datetime

import pandas as pd
from config import *
from PyPDF2 import PdfReader, PdfWriter
from reportlab.lib.pagesizes import A4, landscape
from reportlab.pdfgen import canvas

import utils.excel


def read_data():
    df = pd.read_excel(
        ORDERS_FILE,
        sheet_name='Zamówienia',
        usecols=[3, 4, 5, 8, 10],
    )
    #        skiprows=range(2, 6208),
    df[' order exit date'] = pd.to_datetime(df[' order exit date'], errors='coerce')
    df[' order exit date'] = df[' order exit date'].dt.strftime('%d.%m.%Y')
    input_date = input("Order exit date: ")
    studs = input("0s, 2s, 3s?: ")
    first = True
    save_data = [0, '']
    for index, row in df.iterrows():
        name = row[0]
        order_exit_date = row[4]
        if (order_exit_date == input_date) and ('264' in name and f"{studs[0]} {studs[1].upper()}" in name):
            print(order_exit_date, name)
            card_nr = int(row[2])
            
            if first:
                first = False
                save_data.append(studs)
                save_data.append(input_date)

            save_data[0] += int(row[1])
            save_data[1] += str(card_nr) + ','
    print(save_data)
    write_data(save_data)


def write_data(data):
    try:
        new_qty = data[0]
        card_nrs = data[1]
        studs = data[2]
        input_date_str = data[3]
    except:
        print(f"Zapisz plik {ORDERS_FILE} i spróbuj ponownie")
    '''
    input_format = "%d.%m.%Y"
    output_format = "%Y-%m-%d"

    # Convert input_date to a datetime object using the input_format
    date_obj = datetime.strptime(input_date_str, input_format)
    
    # Format the datetime object using the output_format
    input_date_str = date_obj.strftime(output_format)
    '''
    excel_app = utils.excel.open_excel_app()
    worksheet = utils.excel.open_worksheet(LS_CIE_FILE, excel_app, sheet='Arkusz1')
    print(LS_CIE_FILE, input_date_str)
    df = pd.read_excel(
       LS_CIE_FILE,
       sheet_name='Arkusz1',
    )
    condition = (df['rodzaj detali '].str.contains(studs, na=False)) & (df['ilość do zafakturowania'].notna()) & (df['ilość do zafakturowania'] > 0)
    selected_index = df[condition].index
    date_column = worksheet.Rows(1).Find(input_date_str).Column
    qty_column = worksheet.Rows(1).Find('ilość do zafakturowania').Column
    current_qty = worksheet.Cells(selected_index[0] + 2, qty_column).Value
    if current_qty < new_qty:
        worksheet.Cells(selected_index[0] + 2, date_column).Value = current_qty
        print(int(current_qty))
        ls_nr = str(worksheet.Cells(selected_index[0] + 2, 1).Value)
        n_cards = current_qty // 108
        splt_cards_nr = card_nrs.split(',')
        prev_cards = ''
        for i in range(int(n_cards)):
            prev_cards += splt_cards_nr.pop(0) + ','

        splt_qty = current_qty - n_cards * 108
        prev_cards += f'{splt_cards_nr[0]}({int(splt_qty)}pcs.)'
        worksheet.Cells(selected_index[0] + 3, date_column).Value = prev_cards
        print(prev_cards)
        save_ls(ls_nr, studs, prev_cards, int(current_qty))

        # NEW ROW #
        worksheet.Cells(selected_index[1] + 2, date_column).Value = new_qty - current_qty
        print(int(new_qty - current_qty))
        ls_nr = str(worksheet.Cells(selected_index[1] + 2, 1).Value)
        new_cards = splt_cards_nr.pop(0) + f'({108-int(splt_qty)}pcs.),'
        new_cards += ','.join(splt_cards_nr)
        worksheet.Cells(selected_index[1] + 3, date_column).Value = new_cards[:-1]
        print(new_cards[:-1])
        save_ls(ls_nr, studs, new_cards[:-1], int(new_qty - current_qty))
    else:
        pass
        worksheet.Cells(selected_index[0] + 2, date_column).Value = new_qty
        worksheet.Cells(selected_index[0] + 3, date_column).Value = card_nrs
        ls_nr = str(worksheet.Cells(selected_index[0] + 2, 1).Value)

        save_ls(ls_nr, studs, card_nrs, int(new_qty))
    

def save_ls(nr, studs, cards, quantity):
    files = os.listdir(LS_FOLDER)
    ls_pdf = [file for file in files if nr[:-2] in file and studs.upper() in file and file.endswith('.pdf')]
    print(nr, studs, cards, quantity ,ls_pdf)
    try:
        destination_path = DESKTOP_PATH + '\\' + ls_pdf[0]
    except IndexError:
        print(f"Nie znaleziono pliku zawierającego '{nr[:-2]}', '{studs.upper()} i '.pdf'")
    shutil.copy(LS_FOLDER + '\\' + ls_pdf[0], destination_path)

    page_number = 0
    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=landscape(A4))
    can.saveState()
    can.rotate(90)
    can.setFont("Courier", 20)
    can.drawString(375,-260, str(quantity))
    
    can.setFont("Courier", 10)
    elements = cards.split(',')
    split_size = 15
    split_strings = [elements[i:i+split_size] for i in range(0, len(elements), split_size)]
    move_y = 0
    for split in split_strings:
        if split[-1] == '':
            split.pop()
        new_string = ','.join(split) + ',' if split != split_strings[-1] else ','.join(split)
        can.drawString(50,-350 - move_y, new_string)
        move_y += 20

    can.restoreState()
    can.save()
    packet.seek(0)

    new_pdf = PdfReader(packet)
    with open(destination_path, "rb") as file_obj:
        existing_pdf = PdfReader(file_obj)
        page = existing_pdf.pages[page_number]
        new_pdf = PdfReader(packet)

        page.merge_page(new_pdf.pages[page_number])
        page.rotate(90)

        output = PdfWriter()
        output.add_page(page)

        outputStream = open(destination_path, "wb+")
        output.write(outputStream)
        outputStream.close()


if __name__ == "__main__":
    read_data()

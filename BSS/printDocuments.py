import csv
import io
import os
import re
import sys
import time
from datetime import datetime

from config import *
from PyPDF2 import PdfReader, PdfWriter
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas

sys.path.insert(0, '..')
import shutil

from utils.printer import os_print_pdf, win32_print_pdf


def append_ls_number(file, txt, quantity):
    date_pattern = r'\d{2}\.\d{2}.\d{4}'
    kanban_number_pattern = r'K\d+'
    date = re.findall(date_pattern, file)[0]
    date = datetime.strptime(date, "%d.%m.%Y")
    date = date.strftime("%Y-%m-%d")
    kanban_number = re.findall(kanban_number_pattern, file)[0]

    rows = []
    with open(FOLDER_PATH + '\\'  'kolejność_excel.csv', 'r') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            while len(row) < 9:
                row.append('')

            rows.append(row)

    for i in range(len(rows)):
        if rows[i][0] == date and int(rows[i][4]) == quantity and rows[i][5] == kanban_number[1:] :
            if len(txt) > 9:
                txt = txt[:len(txt) // 2] + '/' + txt[len(txt) // 2:]
            
            rows[i][8] = txt

    with open(FOLDER_PATH + '\\'  'kolejność_excel.csv', 'w+', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(rows)
        

def change_pdf(file, txt):
    page_number = 0
    x_position = 335
    y_position = 415
    font_name = "Courier"
    font_size = 20

    packet = io.BytesIO()
    can = canvas.Canvas(packet, pagesize=A4)
        

    can.setFont(font_name, font_size)

    if len(txt) > 9:
        y_position = 425
        can.drawString(x_position,y_position, txt[:len(txt) // 2])
        y_position = 405
        can.drawString(x_position,y_position, txt[len(txt) // 2:])
    else:
        can.drawString(x_position,y_position, txt)
    can.save()

    packet.seek(0)
    new_pdf = PdfReader(packet)

    with open(PDF_SAVE_PATH + '\\' + file, "rb") as file_obj:
        existing_pdf = PdfReader(file_obj)
        page = existing_pdf.pages[page_number]
        page.merge_page(new_pdf.pages[page_number])
        output = PdfWriter()
        
        output.add_page(page)

        outputStream = open(HDM_OUTPUT_PATH + '\\' + file, "wb+")
        output.write(outputStream)
        outputStream.close()

    try:
        os.remove(PDF_SAVE_PATH + '\\' + file)

    except Exception as e:
        print(f"Wystąpił błąd: {e}")


def move_pdf(file):
    spaces_to_add = max(0, 40 - len(os.path.basename(file)))
    date_str = file[-14:-4]
    date_obj = datetime.strptime(date_str, '%d.%m.%Y')
    week_number = date_obj.strftime('%U')
    year = date_obj.year

    year_dest = NETWORK_KANBAN_FOLDER + '\\' + str(year)
    kw_dest = year_dest + '\\' + f'KW {week_number}' + '\\'

    if not os.path.exists(year_dest):
        print('Utworzono folder ', year_dest)
        os.makedirs(year_dest)
    
    if not os.path.exists(kw_dest):
        print('Utworzono folder ', kw_dest)
        os.makedirs(kw_dest)

    shutil.move(file, kw_dest)
    print(os.path.basename(file) + ' ' * spaces_to_add + ' =>   ' + '\\' + str(year) + '\\' + f'KW {week_number}' + '\\')


if __name__ == "__main__":
    files = os.listdir(PDF_SAVE_PATH)

    zero_s_files = [file for file in files if '0B' in file and file.endswith('.pdf')]
    two_s_files = [file for file in files if '2B' in file and file.endswith('.pdf')]
    three_s_files = [file for file in files if '3B' in file and file.endswith('.pdf')]

    size_0_s  = len(zero_s_files)
    size_2_s = len(two_s_files)
    size_3_s = len(three_s_files)

    og_size_0_s  = len(zero_s_files)
    og_size_2_s = len(two_s_files)
    og_size_3_s = len(three_s_files)

    li_0_s = []
    li_2_s = []
    li_3_s = []

    if size_0_s != 0:
        print(f"\nZnaleziono   {size_0_s}   0S")
        print("Podaj id, pozostałą liczbę części, liczbę części w opakowaniu. Na przykład: 737907654,10000,108")
        input_string = input()
        id = input_string.split(',')[0]
        remaining_quantity = int(input_string.split(',')[1])
        quantity_per_box_zero = int(input_string.split(',')[2])
        how_much_per_id = min(remaining_quantity // quantity_per_box_zero, size_0_s)
        size_0_s -= how_much_per_id
        li_0_s.append([id, how_much_per_id])
        substracted = how_much_per_id * quantity_per_box_zero
        old_id = id
        print(f"\nPozostało {remaining_quantity - substracted} sztuk z karty")
        while size_0_s > 0:
            print("\nPodaj id, pozostałą liczbę części z kolejnej karty. Na przykład: 737907654,10000")
            input_string = input()
            id = input_string.split(',')[0]
            if remaining_quantity - substracted < quantity_per_box_zero and size_0_s != 0:
                li_0_s.append([old_id + id, 1])
                size_0_s -= 1
                remaining_quantity -= 1 * quantity_per_box_zero
            remaining_quantity = int(input_string.split(',')[1]) + (remaining_quantity - substracted)
            how_much_per_id = min(remaining_quantity // quantity_per_box_zero, size_0_s)
            size_0_s -= how_much_per_id
            li_0_s.append([id, how_much_per_id])
            substracted = how_much_per_id * quantity_per_box_zero
            old_id = id  
            print(f"\nPozostało {remaining_quantity - substracted} sztuk z karty")


    if size_2_s != 0:
        print(f"\nZnaleziono   {size_2_s}   2S")
        print("Podaj id, pozostałą liczbę części, liczbę części w opakowaniu. Na przykład: 737907654,10000,108")
        input_string = input()
        id = input_string.split(',')[0]
        remaining_quantity = int(input_string.split(',')[1])
        quantity_per_box_two = int(input_string.split(',')[2])
        how_much_per_id = min(remaining_quantity // quantity_per_box_two, size_2_s)
        size_2_s -= how_much_per_id
        li_2_s.append([id, how_much_per_id])
        substracted = how_much_per_id * quantity_per_box_two
        old_id = id
        print(f"\nPozostało {remaining_quantity - substracted } sztuk z karty")
        while size_2_s > 0:
            print("\nPodaj id, pozostałą liczbę części z kolejnej karty. Na przykład: 737907654,10000")
            input_string = input()
            id = input_string.split(',')[0]
            if remaining_quantity - substracted < quantity_per_box_two and size_2_s != 0:
                li_2_s.append([old_id + id, 1])
                size_2_s -= 1
                remaining_quantity -= 1 * quantity_per_box_two
            remaining_quantity = int(input_string.split(',')[1]) + (remaining_quantity - substracted)
            how_much_per_id = min(remaining_quantity // quantity_per_box_two, size_2_s)
            size_2_s -= how_much_per_id
            li_2_s.append([id, how_much_per_id])
            substracted = how_much_per_id * quantity_per_box_two
            old_id = id  
            print(f"\nPozostało {remaining_quantity - substracted } sztuk z karty")


    if size_3_s != 0:
        print(f"\nZnaleziono   {size_3_s}   3S")
        print("Podaj id, pozostałą liczbę części, liczbę części w opakowaniu. Na przykład: 737907654,10000,108")
        input_string = input()
        id = input_string.split(',')[0]
        remaining_quantity = int(input_string.split(',')[1])
        quantity_per_box_three = int(input_string.split(',')[2])
        how_much_per_id = min(remaining_quantity // quantity_per_box_three, size_3_s)
        size_3_s -= how_much_per_id
        li_3_s.append([id, how_much_per_id])
        substracted = how_much_per_id * quantity_per_box_three
        old_id = id
        print(f"\nPozostało {remaining_quantity - substracted } sztuk z karty")
        while size_3_s > 0:
            print("\nPodaj id, pozostałą liczbę części z kolejnej karty. Na przykład: 737907654,10000")
            input_string = input()
            id = input_string.split(',')[0]
            if remaining_quantity - substracted < quantity_per_box_three and size_3_s != 0:
                li_3_s.append([old_id + id, 1])
                size_3_s -= 1
                remaining_quantity -= 1 * quantity_per_box_three
            remaining_quantity = int(input_string.split(',')[1]) + (remaining_quantity - substracted)
            how_much_per_id = min(remaining_quantity // quantity_per_box_three, size_3_s)
            size_3_s -= how_much_per_id
            li_3_s.append([id, how_much_per_id])
            substracted = how_much_per_id * quantity_per_box_three
            old_id = id  
            print(f"\nPozostało {remaining_quantity - substracted } sztuk z karty")


    print('\n\n')

    i = 0
    for file in zero_s_files:
        txt = li_0_s[0 + i][0]
        print(file, txt)
        append_ls_number(file, txt, quantity_per_box_zero)
        change_pdf(file, txt)
        li_0_s[0 + i][1] -= 1
        if li_0_s[0 + i][1] == 0:
            i += 1
    
    
    i = 0
    for file in two_s_files:
        txt = li_2_s[0 + i][0]
        print(file, txt)
        append_ls_number(file, txt, quantity_per_box_two)
        change_pdf(file, txt)
        li_2_s[0 + i][1] -= 1
        if li_2_s[0 + i][1] == 0:
            i += 1


    i = 0
    for file in three_s_files:
        txt = li_3_s[0 + i][0]
        print(file, txt)
        append_ls_number(file, txt, quantity_per_box_three)
        change_pdf(file, txt)
        li_3_s[0 + i][1] -= 1
        if li_3_s[0 + i][1] == 0:
            i += 1


    hdm_files = os.listdir(HDM_OUTPUT_PATH)
    pdf_files = os.listdir(PDF_SAVE_PATH)
    print("\n\nPiki do drukowania:")
    for file in pdf_files:
        print(file)
    for hdm in hdm_files:
        print(hdm)

    print('<-!-> Dopisz numery LS przed drukowaniem <-!->')
    printing = input("Drukować? (T/N)\n")
    pdf_files = os.listdir(PDF_SAVE_PATH)
    hdm_files = os.listdir(HDM_OUTPUT_PATH)
    if printing == 't' or printing == 'T':
        for file in pdf_files:
            os_print_pdf(PDF_SAVE_PATH + '\\' + file)
            time.sleep(2)
        for hdm in hdm_files:
            os_print_pdf(HDM_OUTPUT_PATH + '\\' + hdm)
            time.sleep(2)
    print("<-!-> Poczekaj na koniec drukowania <-!->")
    end_print = input("Koniec drukowania? (T/N)")
    if end_print == 't' or end_print == 'T':
        for file in pdf_files:
            move_pdf(PDF_SAVE_PATH + '\\' + file)
        for hdm in hdm_files:
            move_pdf(HDM_OUTPUT_PATH + '\\' + hdm)  
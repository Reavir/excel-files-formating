import os
import sys
import smtplib
from datetime import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time

import win32com.client as win32
from config import *

sys.path.insert(0, '..')
import utils.excel


def read_save_data():
    # Initialize Excel application
    excel_app = utils.excel.open_excel_app()

    # Open the workbook
    print('Otwieranie ', ORDERS_FILE)
    ws = utils.excel.open_worksheet(ORDERS_FILE, excel_app, sheet='Zamówienia')
    print('Otwarto ', ORDERS_FILE)

    # Apply autofilter based on user-provided criteria (date filtering in this case)
    ws.AutoFilterMode = False
    filter_column_date = 8
    filter_column_supplier = 10
    filter_date = datetime.now().strftime('%d.%m.%Y')

    for supplier in ALL_SUPPLIERS:
        print('\n             ' + supplier['name'] + '             \n')
        ws.Range('A1').AutoFilter(Field=filter_column_date, Criteria1='=' + filter_date)
        ws.Range('A1').AutoFilter(Field=filter_column_supplier, Criteria1='=' + supplier['name'])

        # Save the filtered sheet as a PDF
        pdf_output = DESKTOP_PATH + '\\' + f'Confirmation {supplier["name"]} {filter_date}.pdf'
        found = False
        # Get the address of the filtered data
        row_index = 6834

        while ws.Cells(row_index, 2).Value is not None:
            pywin_datetime = ws.Cells(row_index, 8).Value
            pywin_datetime_datetime = datetime(pywin_datetime.year, pywin_datetime.month, pywin_datetime.day)
            formatted_date = pywin_datetime_datetime.strftime("%d.%m.%Y")
            print(ws.Cells(row_index, 10).Value, supplier['name'], formatted_date, filter_date)
            if ws.Cells(row_index, 10).Value == supplier['name'] and formatted_date == filter_date:
                found = True
            row_index += 1

        filtered_range = ws.Range(f'A1:J{row_index - 1}')
        ws.PageSetup.Orientation = win32.constants.xlLandscape
        ws.PageSetup.Zoom = False
        ws.PageSetup.FitToPagesTall = 1
        ws.PageSetup.FitToPagesWide = 1

        if found:
            print("Eksportowanie...")
            export_time_start = time.time()
            filtered_range.ExportAsFixedFormat(0, pdf_output)
            print(f"Eksportowanie zajeło: {time.time() - export_time_start} sec.")

            print(f"Zapisano jako {pdf_output}\n")
            for i in supplier.items():
                print(i)
            
            confi = input(f"Wysyłać (T/N)?\n")
            if confi == 't' or confi == 'T':
                send_mail(pdf_output, supplier, filter_date)


def send_mail(pdf_file, supplier, date):
    subject = f"Confirmation {supplier['name']} {date}"

    # Create a MIME multipart message
    msg = MIMEMultipart()
    msg['From'] = SENDER_EMAIL
    msg['To'] = ', '.join(supplier['mail_to'])
    msg['Cc'] = ', '.join(supplier['cc'])
    msg['Subject'] = subject

    # Add email body
    body  = "Message sent automatically.\n\n"
    body += "Z poważaniem/ Mit freundlichen Grüßen/ Best regards\n"

    msg.attach(MIMEText(body, 'plain'))

    # Attach a file
    attachment_path = pdf_file
    attachment_filename = os.path.basename(attachment_path)
    with open(attachment_path, 'rb') as attachment:
        part = MIMEApplication(attachment.read(), Name=attachment_filename)
        part['Content-Disposition'] = f'attachment; filename="{attachment_filename}"'
        msg.attach(part)
    
    # Connect to the SMTP server and send the email
    try:
        server = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
        server.login(SENDER_EMAIL, SENDER_PASSWORD)
        server.sendmail(SENDER_EMAIL, supplier['mail_to']+supplier['cc'], msg.as_string())
        server.quit()
        print("Email wysłano pomyślnie")
    except Exception as e:
        print(f"Wystąpił błąd: {e}")



if __name__ == "__main__":
    read_save_data()

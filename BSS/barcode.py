import base64
import os
import re
import sys
import time

import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

sys.path.insert(0, '..')

from config import *

from utils.printer import os_print_pdf

PDF_FOLDER_PATH = BARCODE_FOLDER_PATH + '\\' + 'PDF'


if __name__ == "__main__":
    html_file_path = BARCODE_FOLDER_PATH + '\\' + "barcode_A4.html"
    pattern = r'var value = "(.*?)"'

    # Browser
    chrome_options = Options()
    chrome_options.add_argument('--kiosk-printing')
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options)
    
    # Codes numbers
    df = pd.read_excel(ORDERS_FILE,
                        sheet_name='Zamówienia',
                          usecols=['order number'],
                           skiprows=range(1, 7315)
                           )

    last_code_nr = max([int(re.match(r'^GTM(\d+)$', str(x)).group(1)) for x in df['order number'] if re.match(r'^GTM(\d+)$', str(x))], default=None)
    
    
    print("Ostatni Kod: " + str(last_code_nr) + " (Ctrl + C jeśli niepoprawny)")
    new_last = int(input("Ile nowych kodów?:\n"))


    # Edit and save HTML as PDF
    for code in range(last_code_nr + 1, last_code_nr + 1 + new_last):
        with open(html_file_path, 'r') as file:
            html_content = file.read()

        new_value = "MRGGTM" + str(code)
        
        modified_html_content = re.sub(pattern, f'var value = "{new_value}"', html_content)
        with open(html_file_path, 'w') as file:
            file.write(modified_html_content)

        print(new_value[0:3], new_value[3:])
        driver.get("file://" + html_file_path)
        pdf_content = driver.execute_cdp_cmd("Page.printToPDF", {})

        with open(PDF_FOLDER_PATH + '\\' + new_value + '.pdf', "wb+") as pdf_file:
            decoded_content = base64.b64decode(pdf_content['data'])
            pdf_file.write(decoded_content)

    # Print PDFs
    pdf_files = os.listdir(PDF_FOLDER_PATH)
    for file in pdf_files:
        os_print_pdf(PDF_FOLDER_PATH + '\\' + file)

    print("PDF-y usunięte za 60s")
    time.sleep(60)
    print("PDF-y usunięte")

    for file in pdf_files:
        os.remove(PDF_FOLDER_PATH + '\\' + file)

    driver.quit()

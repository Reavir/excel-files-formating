import copy
import csv
import os
import sys

import win32com.client as win32
from config import *
from win32com.client import Dispatch


def read_data():
    files = os.listdir(DESKTOP_PATH)
    filtered_files = [file for file in files if file.startswith('DokHandlowe') and file.endswith('.csv')]
    filtered_files.sort(key=lambda x: os.path.getmtime(DESKTOP_PATH + '\\' + x), reverse=True)
    latest_file = filtered_files[0]
    rows = []
    with open(DESKTOP_PATH + '\\' + latest_file, 'r', encoding='utf-16') as file:
        reader = csv.reader(file, delimiter='\t')
        for i, row in enumerate(reader):
            # If incorrect number of columns
            if i == 0 and len(row) != 18:
                print(f"Incorect number of columns, {len(row)} {row}")
                print("Handel")
                print("    |-Sprzedaż")
                print("        |-Faktury Sprzedaży")
                sys.exit()

            cols_to_append = [row[i] for i in [2,3,5,8,9,15]]
            rows.append(cols_to_append)

    # Clean up old files
    for file in filtered_files:
        file_path = DESKTOP_PATH + '\\' + file
        os.remove(file_path)
        print(f"\nDeleted {file_path}\n")

    return rows    


def process_data(data):
    # Change column names
    data[0][0] = 'Numer dokumentu'
    data[0][1] = 'Data wystawienia dokumentu'
    data[0][2] = 'NIP kontrahenta'
    data[0][3] = 'Kwota brutto'
    data[0][4] = 'Waluta'
    data[0][5] = 'Termin płatności'

    name_data = copy.deepcopy(data)
    name_data[0][1] = 'Data wystawienia'
    name_data[0][2] = 'Nazwa kontrahenta'
    name_data[0][4] = ''

    # Swap columns
    # Old columns       [0, 1, 2, 3, 4, 5]
    #                           ⇓
    new_col_positions = [2, 0, 1, 5, 4, 3]
    for sublist in data:
        sublist_copy = sublist.copy()
        for new_index, old_index in enumerate(new_col_positions):
            sublist[new_index] = sublist_copy[old_index]
    
    # Name to NIP
    nip_data = copy.deepcopy(data)
    print('Do importu...')
    print(nip_data[0])
    for row in nip_data[1:]:
        row[0] = BRUSS_NIP.get(row[0], 'Brak danych')
        print(row)

    return name_data, nip_data


def write_data_excel(name_data):
    excel_app = Dispatch('Excel.Application')
    excel_app.Visible = False
    
    workbook = excel_app.Workbooks.Add()
    worksheet = workbook.Worksheets(1)
    
    worksheet.Columns('A').ColumnWidth = 18.29
    worksheet.Columns('B').ColumnWidth = 15.86
    worksheet.Columns('C').ColumnWidth = 58.86
    worksheet.Columns('D').ColumnWidth = 12
    worksheet.Columns('E').ColumnWidth = 6.71
    worksheet.Columns('F').ColumnWidth = 15.29
    print('\n' + 'Do druku...')
    for row_index, row_data in enumerate(name_data, start=1):
        print(row_data)
        for col_index, value in enumerate(row_data, start=1):
            cell = worksheet.Cells(row_index, col_index)
            cell.Value = value
            if col_index in [2,4,6]:
                cell.HorizontalAlignment = win32.constants.xlRight
                
    save_path = FOLDER_PATH + rf'\toPrint.xlsx'
    print('\n' + 'Saving... ')
    print(save_path)

    workbook.SaveAs(save_path)
    workbook.Close()


def write_data_csv(nip_data):
    print('\n' + 'Saving... ')
    print( FOLDER_PATH + rf'\FactDoc.csv')
    with open(FOLDER_PATH + rf'\FactDoc.csv', 'w+', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(nip_data)

    print('\n')

if __name__ == "__main__":
    data = read_data()
    n_data, np_data = process_data(data)
    write_data_excel(n_data)
    write_data_csv(np_data)
